import React, {Component} from 'react';
import './App.css';
import TopNavbar from './components/TopNavbar/TopNavbar';
import SideDrawer from './components/SideDrawer/SideDrawer'
import BackDrop from './components/BackDrop/BackDrop';
import CarouselComponent from './components/CarouselComponent/CarouselComponent'
import WorkComponent from './components/Work/Work';
import PracticeComponent from './components/PracticeArea/PracticeArea';
import AboutComponent from './components/About/About';
import Contactus from './components/ContactUs/Contactus';
import Footer from './components/Footer/Footer';
import Appointment from './components/Appointment/Appointment';
import AdminLogin from './components/AdminLogin/AdminLogin';
import AdminPanel from './components/AdminPanel/AdminPanel';
import Blog from './components/Blog/Blog';
import LinkPractice from './components/LinkPracticeArea/LinkPractice';
import LinkBlog from './components/LinkBlog/LinkBlog';

class App extends Component {
    constructor(props) {
        super(props);
        let val = localStorage.getItem("auth");
        let logData;
        let link;
        if(val === 'true'){
            logData=JSON.parse(localStorage.getItem("data"))
            link='adminpanel'
        }else {
            link='app'
        }
        this.state = {
            sideDrawerOpen: false,
            open: false,
            logOpen: false,
            appRouter: link,
            logData: logData,
            uname: '',
            lname: '',
            fname: '',
            hm:false,
            pr:false,
            blg:false,
            abt:false,
            cntc:false
        }
    }

    appRouter = (key)=> {
        if (key === 'app'){
            this.setState({hm:true,pr:false,blg:false,abt:false,cntc:false})
        } else if(key === 'practice'){
            this.setState({hm:false,pr:true,blg:false,abt:false,cntc:false})
        }else if(key === 'blogs'){
            this.setState({hm:false,pr:false,blg:true,abt:false,cntc:false})
        }else if(key === 'about'){
            this.setState({hm:false,pr:false,blg:false,abt:true,cntc:false})
        }else if(key === 'contactus'){
            this.setState({hm:false,pr:false,blg:false,abt:false,cntc:true})
        }
        this.setState({appRouter: key})
    }
    onOpenModal = () => {
        this.setState({open: true});
    };

    onCloseModal = () => {
        this.setState({open: false});
    };

    onOpenModalLog = () => {
        this.setState({logOpen: true});
    };

    onCloseModalLog = () => {
        this.setState({logOpen: false});
    };

    handelSideDrawer = () => {
        this.setState((prevState)=> {
            return {sideDrawerOpen: !prevState.sideDrawerOpen}
        })
    }

    handelBackDrop = ()=> {
        this.setState({sideDrawerOpen: false})
    }

    getLogdata = (value)=> {
        this.setState({logData: value});
        localStorage.setItem("auth", "true");
        localStorage.setItem("data", JSON.stringify(value))
    }

    render() {
        let backdrop;
        if (this.state.sideDrawerOpen) {
            backdrop = <BackDrop closeBackDrop={this.handelBackDrop}/>
        }
        return (
            <div >
                {this.state.appRouter === 'app' ?
                    <div>
                        <TopNavbar
                            drawerHandler={this.handelSideDrawer}
                            openModal={this.onOpenModal}
                            openLog={this.onOpenModalLog}
                            router={this.appRouter}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        <SideDrawer
                            show={this.state.sideDrawerOpen}
                            router={this.appRouter}
                            openModal={this.onOpenModal}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        {backdrop}
                        <main>
                            <CarouselComponent
                                openModal={this.onOpenModal}
                                router={this.appRouter}
                            />
                            <WorkComponent/>
                            <PracticeComponent
                                router={this.appRouter}
                            />
                            <Blog
                                router={this.appRouter}
                            />
                            <AboutComponent/>
                            <Contactus/>
                            <Footer/>
                            <Appointment
                                open={this.state.open}
                                closeModal={this.onCloseModal}
                            />
                            <AdminLogin
                                openLog={this.state.logOpen}
                                closeLog={this.onCloseModalLog}
                                data={this.getLogdata}
                                onRouter={this.appRouter}
                            />
                        </main>
                    </div> : null
                }

                {this.state.appRouter === 'adminpanel' ?
                    <AdminPanel
                        data={this.state.logData}
                        router={this.appRouter}
                    /> : null
                }

                {this.state.appRouter === 'practice' ?
                    <div>
                        <TopNavbar
                            drawerHandler={this.handelSideDrawer}
                            openModal={this.onOpenModal}
                            openLog={this.onOpenModalLog}
                            router={this.appRouter}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        <SideDrawer
                            show={this.state.sideDrawerOpen}
                            router={this.appRouter}
                            openModal={this.onOpenModal}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        {backdrop}
                        <LinkPractice/>
                        <Footer/>
                        <Appointment
                            open={this.state.open}
                            closeModal={this.onCloseModal}
                        />
                    </div>
                    :
                    null
                }
                {this.state.appRouter === 'blogs' ?
                    <div>
                        <TopNavbar
                            drawerHandler={this.handelSideDrawer}
                            openModal={this.onOpenModal}
                            openLog={this.onOpenModalLog}
                            router={this.appRouter}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        <SideDrawer
                            show={this.state.sideDrawerOpen}
                            router={this.appRouter}
                            openModal={this.onOpenModal}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        {backdrop}
                        <LinkBlog/>
                        <Footer/>
                        <Appointment
                            open={this.state.open}
                            closeModal={this.onCloseModal}
                        />
                    </div>
                    :
                    null
                }
                {this.state.appRouter === 'about' ?
                    <div>
                        <TopNavbar
                            drawerHandler={this.handelSideDrawer}
                            openModal={this.onOpenModal}
                            openLog={this.onOpenModalLog}
                            router={this.appRouter}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        <SideDrawer
                            show={this.state.sideDrawerOpen}
                            router={this.appRouter}
                            openModal={this.onOpenModal}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        {backdrop}
                        <AboutComponent/>
                        <Footer/>
                        <Appointment
                            open={this.state.open}
                            closeModal={this.onCloseModal}
                        />
                    </div>
                    :
                    null
                }
                {this.state.appRouter === 'contactus' ?
                    <div>
                        <TopNavbar
                            drawerHandler={this.handelSideDrawer}
                            openModal={this.onOpenModal}
                            openLog={this.onOpenModalLog}
                            router={this.appRouter}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        <SideDrawer
                            show={this.state.sideDrawerOpen}
                            router={this.appRouter}
                            openModal={this.onOpenModal}
                            home={this.state.hm}
                            pr={this.state.pr}
                            blg={this.state.blg}
                            abt={this.state.abt}
                            cntc={this.state.cntc}
                        />
                        {backdrop}
                        <Contactus/>
                        <Footer/>
                        <Appointment
                            open={this.state.open}
                            closeModal={this.onCloseModal}
                        />
                    </div>
                    :
                    null
                }
            </div>
        );
    }
}
export default App;