import React from 'react';
import './DrawerToggelButton.css'

class DrawerToggelButton extends React.Component{
    render(props){
        return(
            <button className="toggel-btn" onClick={this.props.clickDrawer}>
                <div className="toggel-btn-line"/>
                <div className="toggel-btn-line"/>
                <div className="toggel-btn-line"/>
            </button>
        )
    }
}
export default DrawerToggelButton;