import React from 'react';
import './SideDrawer.css';

class SideDrawer extends React.Component{
    componentDidMount(){
        window.scrollTo(0,0);
    }
    render(){
        let sidedraw = 'side-drawer'
        if(this.props.show){
            sidedraw = 'side-drawer open'
        }
        return(
            <nav className={sidedraw}>
                <ul>
                    <li onClick={()=>this.props.router('app')} className={`${this.props.home?'active':null}`}>home</li>
                    <li onClick={()=>this.props.router('practice')} className={`${this.props.pr?'active':null}`}>practice area</li>
                    <li onClick={()=>this.props.router('blogs')} className={`${this.props.blg?'active':null}`}>blog</li>
                    <li onClick={()=>this.props.router('about')} className={`${this.props.abt?'active':null}`}>about</li>
                    <li onClick={()=>this.props.router('contactus')} className={`${this.props.cntc?'active':null}`}>contact</li>
                    <li className="btn-map">
                        <button className="aa">
                                    <span onClick={this.props.openModal}>
                                        make an appointment
                                    </span>
                        </button>
                    </li>
                </ul>
            </nav>
        )
    }
}

export default SideDrawer;