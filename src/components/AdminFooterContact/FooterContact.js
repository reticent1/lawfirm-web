import React from 'react';
import './FooterContact.css';
import Modal from 'react-responsive-modal';
import {NotificationManager, NotificationContainer} from 'react-notifications';

class FooterContact extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addr: '',
            mob: '',
            email: '',
            web: '',
            adblnk: true,
            advalid: true,
            mobblnk: true,
            mobvalid: true,
            mailblnk: true,
            mailvalid: true,
            wbblnk: true,
            wbvalid: true,
            id:''
        }
    }

    getAddress = (event)=> {
        this.setState({addr: event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({adblnk: true})
            if (event.target.value.length > 200) {
                this.setState({advalid: false})
            } else {
                this.setState({advalid: true})
            }
        } else {
            this.setState({adblnk: false})
        }
    }
    getMob = (event)=> {
        const mobValidator = /^\d{9}$/;
        this.setState({mob: event.target.value})
        if (event.target.value.match(mobValidator)) {
            this.setState({
                mobvalid: true,
            })
        } else if (event.target.value.trim() === "") {
            this.setState({mobblnk: false, mobvalid: true})
        } else {
            this.setState({mobvalid: false, mobblnk: true})
        }
    }
    getMail = (event)=> {
        this.setState({email: event.target.value})
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (event.target.value.match(mailformat)) {
            this.setState({mailvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({mailblnk: false, mailvalid: true})
        } else {
            this.setState({mailvalid: false, mailblnk: true})
        }
    }
    getWeb = (event)=> {
        const webUrl= /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/;
        this.setState({web: event.target.value})
        if (event.target.value.match(webUrl)) {
            this.setState({wbvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({wbblnk: false, wbvalid: true})
        } else {
            this.setState({wbvalid: false, wbblnk: true})
        }
    }
    getVal = ()=> {
        let addr, mob, email, web,id;
        this.props.data.map(data=> {
            return (
                addr = data.address,
                    mob = data.mobile,
                    email = data.email,
                    web = data.website,
                    id=data.id
            )
        })
        this.setState({
            addr: addr,
            mob: mob,
            email: email,
            web: web,
            adblnk: true,
            advalid: true,
            mobblnk: true,
            mobvalid: true,
            mailblnk: true,
            mailvalid: true,
            wbblnk: true,
            wbvalid: true,
            id:id
        })
    }
    updateContact = ()=> {
        const mobValidator = /^\d{9}$/;
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        const {addr, mob, web, email,id}=this.state;
        if (addr.trim() !== "" && mob.match(mobValidator) && web.trim() !== "" && email.match(mailformat) && addr.length <= 200) {
            fetch('https://secure-wave-27786.herokuapp.com/contactinfo/info/update', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    id: id,
                    address: this.state.addr,
                    mobile: this.state.mob,
                    email: this.state.email,
                    website: this.state.web
                })
            })
                .then(response=>response.json())
                .then(data=> {
                    if (data === 'error in update') {
                        NotificationManager.error('Sorry! Contact info not updated.')
                    } else {
                        this.props.close();
                        this.setState({
                            adblnk: true,
                            advalid: true,
                            mobblnk: true,
                            mobvalid: true,
                            mailblnk: true,
                            mailvalid: true,
                            wbblnk: true,
                            wbvalid: true
                        })
                        NotificationManager.success('Contact info Updated successfully!')
                    }
                })
                .catch(console.log)
        }
        if (addr.trim() === "") {
            this.setState({adblnk: false})
        } else if (mob.trim() === "") {
            this.setState({mobblnk: false})
        } else if (web.trim() === "") {
            this.setState({wbblnk: false})
        } else if (email.trim() === "") {
            this.setState({mailblnk: false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    onEntered={this.getVal}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div className="con-mod-container">
                        <h2>Update Contact Information {this.state.id}</h2>
                        <textarea type="text" placeholder="Address" cols="30" rows="5" value={this.state.addr}
                                  className="con-control" onChange={this.getAddress}/>
                        <input type="text" placeholder="Contact No." value={this.state.mob} className="con-control"
                               onChange={this.getMob}/>
                        <input type="text" placeholder="Email Address" value={this.state.email} className="con-control"
                               onChange={this.getMail}/>
                        <input type="text" placeholder="Website url" value={this.state.web} className="con-control"
                               onChange={this.getWeb}/>
                        <button onClick={this.updateContact}>Update</button>
                    </div>
                    <span hidden={this.state.adblnk} className="error-span">Please enter address</span>
                    <span hidden={this.state.advalid} className="error-span">Please enter address within 200 characters</span>
                    <span hidden={this.state.mobblnk} className="error-span">Please enter contact no.</span>
                    <span hidden={this.state.mobvalid} className="error-span">Please enter valid contact no.</span>
                    <span hidden={this.state.mailblnk} className="error-span">Please enter email id.</span>
                    <span hidden={this.state.mailvalid} className="error-span">Please enter valid email id.</span>
                    <span hidden={this.state.wbblnk} className="error-span">Please enter website.</span>
                    <span hidden={this.state.wbvalid} className="error-span">Please enter valid url of website.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default FooterContact;