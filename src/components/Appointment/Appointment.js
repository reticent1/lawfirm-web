import React from 'react';
import './Appointment.css';
import Modal from 'react-responsive-modal';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import {SingleDatePicker} from 'react-dates';
import '../css/animate.css';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class Appointment extends React.Component {

    constructor() {
        super();
        this.state = {
            date: null,
            focused: null,
            fname: '',
            lname: '',
            mobile_no: '',
            emailid: '',
            message: '',
            emailvalid: true,
            emailblank: true,
            fnblank: true,
            lnblank: true,
            mobblank: true,
            mobvalid: true,
            lnvalid: true,
            fnvalid: true,
            dtblank:true
        }
    }

    fname = (event)=> {
        // this.setState({fname:event.target.value});
        if (event.target.value.trim() === "") {
            this.setState({fnblank: false, fnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({fnblank: true})
            if (event.target.value.length < 3) {
                this.setState({fnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({fnvalid: true, fname: event.target.value})
            }
        }
    }

    lname = (event)=> {
        // this.setState({lname:event.target.value});
        this.setState({lname: event.target.value})
        if (event.target.value.trim() === "") {
            this.setState({lnblank: false, lnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({lnblank: true})
            if (event.target.value.length < 3) {
                this.setState({lnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({lnvalid: true, lname: event.target.value})
            }
        }
    }

    mobileno = (event)=> {
        const mobValidator = /^\d{9}$/;
        this.setState({mobile_no: event.target.value})
        if (event.target.value.match(mobValidator)) {
            this.setState({
                mobile_no: event.target.value,
                mobvalid: true,
            })
        } else if (event.target.value.trim() === "") {
            this.setState({mobblank: false, mobvalid: true})
        } else {
            this.setState({mobvalid: false, mobblank: true})
        }
    }

    emailid = (event)=> {
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        this.setState({emailid: event.target.value})
        if (event.target.value.match(mailformat)) {
            this.setState({emailvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({emailblank: false, emailvalid: true})
        } else {
            this.setState({emailvalid: false, emailblank: true})
        }
    }

    message = (event)=> {
        this.setState({message: event.target.value});
    }

    handleDateChange = (date)=> {
        this.setState({date: date})
        if (date === ""){
            this.setState({dtblank:false})
        }else {
            this.setState({dtblank:true})
        }
    }

    bookAppointment = ()=> {
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        const mobValidator = /^\d{9}$/;
        const {fname, lname, mobile_no, emailid,date}=this.state;
        if (fname === "") {
            this.setState({fnblank: false, fnvalid: true})
        } else if (lname === "") {
            this.setState({lnblank: false, lnvalid: true})
        }else if (date === null){
            this.setState({dtblank:false})
        } else if (mobile_no === "") {
            this.setState({mobblank: false, mobvalid: true})
        } else if (emailid === "") {
            this.setState({emailblank: false, emailvalid: true})
        }
        if (mobile_no.match(mobValidator) && emailid.match(mailformat) && fname !== '' && lname !== '' && date !== null) {
            fetch('https://secure-wave-27786.herokuapp.com/books', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    fname: this.state.fname,
                    lname: this.state.lname,
                    date: this.state.date,
                    mobile_no: this.state.mobile_no,
                    emailid: this.state.emailid,
                    message: this.state.message
                })
            })
                .then(response => response.json())
                .then(data=> {
                    if (data === 'error') {
                        console.log('error')
                    } else {
                        NotificationManager.info('Appointment Request send successfully.')
                        this.props.closeModal()
                        this.setState({
                            date: null,
                            fname: '',
                            lname: '',
                            mobile_no: '',
                            emailid: '',
                            message: ''
                        })
                    }
                })
                .catch(console.log)
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.closeModal}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn',
                        transitionEnter: 'transitionEnter-an',
                        transitionEnterActive: 'transitionEnterActive-an',
                        transitionExit: 'transitionExit',
                        transitionExitActive: 'transitionExitActive'
                    }}
                    animationDuration={1000}
                >
                    <div>
                        <span className="form-head">
                            Appointment Details
                        </span>
                        <div className="form2-content">
                            <input type="text" className="name-control" placeholder="First Name" onChange={this.fname}/>
                            <input type="text" className="name-control" placeholder="Last Name" onChange={this.lname}/>
                        </div>
                        <div className="form2-content">
                            <SingleDatePicker
                                showClearDate={true}
                                small={true}
                                block={false}
                                regular={false}
                                numberOfMonths={1}
                                date={this.state.date}
                                onDateChange={date => this.handleDateChange(date)}
                                focused={this.state.focused}
                                onFocusChange={({focused}) =>
                                    (this.setState({focused}))
                                }
                                displayFormat="DD/MM/YYYY"
                                openDirection="down"
                                hideKeyboardShortcutsPanel={true}
                                showDefaultInputIcon={true}
                                reopenPickerOnClearDate={true}
                                readOnly={true}
                            />

                        </div>
                        <div className="form2-content">
                            <input type="text" className="form2-control" placeholder="Mobile Number"
                                   onChange={this.mobileno}/>
                        </div>
                        <div className="form2-content">
                            <input type="email" className="form2-control" placeholder="Email Address"
                                   onChange={this.emailid}/>
                        </div>
                        <div className="form2-content">
                            <textarea cols="30" rows="5" className="form2-control" placeholder="Message for us"
                                      onChange={this.message}/>
                        </div>
                        <div className="form2-content">
                            <button className="appo-btn" onClick={this.bookAppointment}>Book</button>
                        </div>
                    </div>
                    <span hidden={this.state.emailblank} className="error-span">Please enter Email Id.</span>
                    <span hidden={this.state.emailvalid} className="error-span">Please enter correct Email Id.</span>
                    <span hidden={this.state.fnblank} className="error-span">Please enter First Name.</span>
                    <span hidden={this.state.lnblank} className="error-span">Please enter Last Name.</span>
                    <span hidden={this.state.mobblank} className="error-span">Please enter Mobile No.</span>
                    <span hidden={this.state.fnvalid} className="error-span">First Name must be 3 or more character.</span>
                    <span hidden={this.state.lnvalid} className="error-span">Last Name must be 3 or more character.</span>
                    <span hidden={this.state.mobvalid} className="error-span">Please enter valid Mobile No.</span>
                    <span hidden={this.state.dtblank} className="error-span">Please select Date.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}

export default Appointment;