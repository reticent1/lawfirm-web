import React from 'react';
import './AdminSettings.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class AdminSettings extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            fname: '',
            lname: '',
            email: '',
            uname: '',
            oldPass: '',
            newPass: '',
            cnfPass: '',
            oldPkey: '',
            newPkey: '',
            cnfPkey: '',
            id: '',
            fnblnk: true,
            fnvalid: true,
            lnblnk: true,
            lnvalid: true,
            mailblnk: true,
            mailvalid: true,
            unblnk: true,
            unvalid: true,
            olblnk: true,
            olvalid: true,
            nwblnk: true,
            nwvalid: true,
            cnfblnk: true,
            cnfvalid: true,
            olnwvalid: true,
            olkbl: true,
            olkval: true,
            nwkbl: true,
            nwkval: true,
            cnkbl: true,
            cnkval: true,
            oknkval: true
        }
    }

    componentDidMount() {
        fetch('https://secure-wave-27786.herokuapp.com/adminlogin/getuser', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                uname: this.props.data.uname
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({data: data})
                let uname, fname, lname, email, id;
                this.state.data.map(data=> {
                    return (
                        uname = data.uname,
                            fname = data.fname,
                            lname = data.lname,
                            email = data.email,
                            id = data.id
                    )
                })
                this.setState({
                    fname: fname,
                    uname: uname,
                    lname: lname,
                    email: email,
                    id: id,
                    fnblnk: true,
                    fnvalid: true,
                    lnblnk: true,
                    lnvalid: true,
                    mailblnk: true,
                    mailvalid: true,
                    unblnk: true,
                    unvalid: true,
                    olblnk: true,
                    olvalid: true,
                    nwblnk: true,
                    nwvalid: true,
                    cnfblnk: true,
                    cnfvalid: true,
                    olnwvalid: true,
                    olkbl: true,
                    olkval: true,
                    nwkbl: true,
                    nwkval: true,
                    cnkbl: true,
                    cnkval: true
                })
            })
            .catch(console.log)
    }

    getFname = (event)=> {
        this.setState({fname: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({fnblnk: true})
            if (event.target.value.length < 3) {
                this.setState({fnvalid: false})
            } else {
                this.setState({fnvalid: true})
            }
        } else {
            this.setState({fnblnk: false, fnvalid: true})
        }
    }
    getLname = (event)=> {
        this.setState({lname: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({lnblnk: true})
            if (event.target.value.length < 3) {
                this.setState({lnvalid: false})
            } else {
                this.setState({lnvalid: true})
            }
        } else {
            this.setState({lnblnk: false, lnvalid: true})
        }
    }
    getUname = (event)=> {
        this.setState({uname: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({unblnk: true})
            if (event.target.value.length < 8) {
                this.setState({unvalid: false})
            } else {
                this.setState({unvalid: true})
            }
        } else {
            this.setState({unblnk: false, unvalid: true})
        }
    }
    getEmail = (event)=> {
        this.setState({email: event.target.value})
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (event.target.value.match(mailformat)) {
            this.setState({mailvalid: true, mailblnk: true})
        } else if (event.target.value.trim() === "") {
            this.setState({mailblnk: false, mailvalid: true})
        } else {
            this.setState({mailvalid: false, mailblnk: true})
        }
    }
    getOldPass = (event)=> {
        this.setState({oldPass: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({olblnk: true})
            if (event.target.value.length < 8) {
                this.setState({olvalid: false})
            } else {
                this.setState({olvalid: true})
            }
        } else {
            this.setState({olblnk: false, olvalid: true})
        }
    }
    getNewPass = (event)=> {
        this.setState({newPass: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({nwblnk: true, olnwvalid: true})
            if (event.target.value.length < 8) {
                this.setState({nwvalid: false, olnwvalid: true})
            } else {
                this.setState({nwvalid: true, olnwvalid: true})
            }
        } else {
            this.setState({nwblnk: false, nwvalid: true, olnwvalid: true})
        }
    }
    getCnfPass = (event)=> {
        this.setState({cnfPass: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({cnfblnk: true, olnwvalid: true})
            if (event.target.value.length < 8) {
                this.setState({cnfvalid: false, olnwvalid: true})
            } else {
                this.setState({cnfvalid: true, olnwvalid: true})
            }
        } else {
            this.setState({cnfblnk: false, cnfvalid: true, olnwvalid: true})
        }
    }
    getOldPkey = (event)=> {
        this.setState({oldPkey: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({olkbl: true})
            if (event.target.value.length < 6) {
                this.setState({olkval: false})
            } else {
                this.setState({olkval: true})
            }
        } else {
            this.setState({olkbl: false, olkval: true})
        }
    }
    getNewPkey = (event)=> {
        this.setState({newPkey: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({nwkbl: true,oknkval:true})
            if (event.target.value.length < 6) {
                this.setState({nwkval: false,oknkval:true})
            } else {
                this.setState({nwkval: true,oknkval:true})
            }
        } else {
            this.setState({nwkbl: false, nwkval: true,oknkval:true})
        }
    }
    getCnfPkey = (event)=> {
        this.setState({cnfPkey: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({cnkbl: true,oknkval:true})
            if (event.target.value.length < 6) {
                this.setState({cnkval: false,oknkval:true})
            } else {
                this.setState({cnkval: true,oknkval:true})
            }
        } else {
            this.setState({cnkbl: false, cnkval: true,oknkval:true})
        }
    }

    onUpdatePassword = ()=> {
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        const {fname, lname, email, uname, oldPass, newPass, cnfPass}=this.state;
        if (this.state.newPass === this.state.cnfPass) {
            this.setState({olnwvalid: true})
            if (fname.trim() !== "" && fname.length >= 3 && lname.trim() !== "" && lname.length >= 3 &&
                email.match(mailformat) && uname.trim() !== "" && uname.length >= 8 && oldPass.trim() !== "" &&
                oldPass.length >= 8 && newPass.trim() !== "" && newPass.length >= 8 && cnfPass.trim() !== "" && cnfPass.length >= 8) {
                fetch('https://secure-wave-27786.herokuapp.com/adminlogin/update/password', {
                    method: 'post',
                    headers: {'content-type': 'application/json'},
                    body: JSON.stringify({
                        id: this.state.id,
                        fname: this.state.fname,
                        lname: this.state.lname,
                        uname: this.state.uname,
                        password: this.state.oldPass,
                        newpassword: this.state.newPass,
                        email: this.state.email
                    })
                })
                    .then(response=>response.json())
                    .then(data=> {
                        if (data === 'old password not matched') {
                            NotificationManager.error('Old password not matched.')
                        } else if (data === 'not updated') {
                            NotificationManager.error('Error! Password not updated.')
                        } else if (data === 'existed') {
                            NotificationManager.info('Email existed already.')
                        } else {
                            NotificationManager.success('Password Updated successfully!')
                            localStorage.clear();
                            this.props.router('app');
                            this.setState({
                                fnblnk: true,
                                fnvalid: true,
                                lnblnk: true,
                                lnvalid: true,
                                mailblnk: true,
                                mailvalid: true,
                                unblnk: true,
                                unvalid: true,
                                olblnk: true,
                                olvalid: true,
                                nwblnk: true,
                                nwvalid: true,
                                cnfblnk: true,
                                cnfvalid: true,
                                olnwvalid: true,
                                oldPass: '',
                                newPass: '',
                                cnfPass: '',
                            })
                        }
                    })
                    .catch(console.log)
            }
        } else {
            this.setState({olnwvalid: false})
        }
        if (fname.trim() === "") {
            this.setState({fnblnk: false})
        } else if (lname.trim() === "") {
            this.setState({lnblnk: false})
        } else if (email.trim() === "") {
            this.setState({mailblnk: false})
        } else if (uname.trim() === "") {
            this.setState({unblnk: false})
        } else if (oldPass.trim() === "") {
            this.setState({olblnk: false})
        } else if (newPass.trim() === "") {
            this.setState({nwblnk: false})
        } else if (cnfPass.trim() === "") {
            this.setState({cnfblnk: false})
        }
    }
    onUpdatePasskey = ()=> {
        const {oldPkey, newPkey, cnfPkey}=this.state;
        if (this.state.newPkey === this.state.cnfPkey) {
            this.setState({oknkval: true})
            if (oldPkey.trim() !== "" && oldPkey.length >= 6 && newPkey.trim() !== "" && newPkey.length >= 6 &&
                cnfPkey.trim() !== "" && cnfPkey.length >= 6) {
                fetch('https://secure-wave-27786.herokuapp.com/adminlogin/update/passkey', {
                    method: 'post',
                    headers: {'content-type': 'application/json'},
                    body: JSON.stringify({
                        id: this.state.id,
                        passkey: this.state.oldPkey,
                        newpasskey: this.state.newPkey
                    })
                })
                    .then(response=>response.json())
                    .then(data=> {
                        if (data === 'old passkey not matched') {
                            NotificationManager.error('Old passkey incorrect.')
                        } else if (data === 'not updated') {
                            NotificationManager.error('Passkey not updated.')
                        } else {
                            NotificationManager.success('Passkey Updated Successfully.')
                            localStorage.clear();
                            this.props.router('app');
                            this.setState({
                                oldPkey: '', newPkey: '', olkbl: true,
                                olkval: true,
                                nwkbl: true,
                                nwkval: true,
                                cnkbl: true,
                                cnkval: true,
                                oknkval: true
                            })
                        }
                    })
                    .catch(console.log)
            }
        } else {
            this.setState({oknkval: false})
        }
        if (oldPkey.trim() === "") {
            this.setState({olkbl: false})
        } else if (newPkey.trim() === "") {
            this.setState({nwkbl: false})
        } else if (cnfPkey.trim() === "") {
            this.setState({cnkbl: false})
        }
    }

    render() {
        return (
            <div className="ad-set-container">
                <div className="shadow-div">
                    <div className="ad-set-content">
                        <h1>Personal Info</h1>
                        <label>First Name</label>
                        <input type="text" placeholder="First Name" value={this.state.fname} onChange={this.getFname}/>
                    </div>
                    <div className="ad-set-content">
                        <label>Last Name</label>
                        <input type="text" placeholder="Last Name" value={this.state.lname} onChange={this.getLname}/>
                    </div>
                    <div className="ad-set-content">
                        <label>Email Id</label>
                        <input type="email" placeholder="Email Id" value={this.state.email} onChange={this.getEmail}/>
                    </div>
                    <div className="ad-set-content">
                        <label>Username</label>
                        <input type="text" placeholder="Username" value={this.state.uname} onChange={this.getUname}/>
                    </div>
                    <div className="ad-set-content">
                        <label>Old Password</label>
                        <input type="password" placeholder="Old Password" onChange={this.getOldPass}/>
                    </div>
                    <div className="ad-set-content">
                        <label>New Password</label>
                        <input type="password" placeholder="New Password" onChange={this.getNewPass}/>
                    </div>
                    <div className="ad-set-content">
                        <label>Confirm New Password</label>
                        <input type="password" placeholder="Confirm New Password" onChange={this.getCnfPass}/>
                    </div>
                    <div className="ad-set-content">
                        <button onClick={this.onUpdatePassword}>Save Changes</button>
                    </div>
                    <div className="ad-set-content">
                        <span hidden={this.state.fnblnk} className="error-span-vl">Please enter First Name.</span>
                        <span hidden={this.state.fnvalid} className="error-span">First Name must be greater than 3 characters.</span>
                        <span hidden={this.state.lnblnk} className="error-span">Please enter Last Name.</span>
                        <span hidden={this.state.lnvalid} className="error-span">Last Name must be greater than 3 characters.</span>
                        <span hidden={this.state.mailblnk} className="error-span">Please enter email id.</span>
                        <span hidden={this.state.mailvalid} className="error-span">Please enter valid email id.</span>
                        <span hidden={this.state.unblnk} className="error-span">Please enter Username.</span>
                        <span hidden={this.state.unvalid} className="error-span">Please enter username more than 8 characters.</span>
                        <span hidden={this.state.olblnk} className="error-span">Please enter Old password.</span>
                        <span hidden={this.state.olvalid} className="error-span">Please enter old password more than 8 characters.</span>
                        <span hidden={this.state.nwblnk} className="error-span">Please enter new password.</span>
                        <span hidden={this.state.nwvalid} className="error-span">Please enter new password more than 8 characters.</span>
                        <span hidden={this.state.cnfblnk} className="error-span">Please enter confirm password.</span>
                        <span hidden={this.state.cnfvalid} className="error-span">Please enter confirm password more than 8 characters.</span>
                        <span hidden={this.state.olnwvalid}
                              className="error-span">New & Confirm Password not matched.</span>
                    </div>
                </div>
                <div className="shadow-div">
                    <div className="ad-set-content">
                        <h1>Change PassKey</h1>
                        <label>Old Passkey</label>
                        <input type="password" placeholder="Old Password" onChange={this.getOldPkey}/>
                    </div>
                    <div className="ad-set-content">
                        <label>New Passkey</label>
                        <input type="password" placeholder="New Password" onChange={this.getNewPkey}/>
                    </div>
                    <div className="ad-set-content">
                        <label>Confirm New Passkey</label>
                        <input type="password" placeholder="Confirm New Password" onChange={this.getCnfPkey}/>
                    </div>
                    <div className="ad-set-content">
                        <button onClick={this.onUpdatePasskey}>Save Changes</button>
                    </div>
                    <div className="ad-set-content">
                        <span hidden={this.state.olkbl} className="error-span-vl">Please enter Old key</span>
                        <span hidden={this.state.olkval} className="error-span-vl">Please enter Old key more than 6 characters</span>
                        <span hidden={this.state.nwkbl} className="error-span-vl">Please enter New key</span>
                        <span hidden={this.state.nwkval} className="error-span-vl">Please enter New key more than 6 characters</span>
                        <span hidden={this.state.cnkbl} className="error-span-vl">Please enter Confirm key</span>
                        <span hidden={this.state.cnkval} className="error-span-vl">Please enter confrim key more than 6 characters</span>
                        <span hidden={this.state.oknkval} className="error-span-vl">New & Old key not matched.</span>
                    </div>
                </div>
                <NotificationContainer/>
            </div>
        );
    }

}
export default AdminSettings;