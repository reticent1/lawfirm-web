import React from 'react';
import Modal from 'react-responsive-modal';
import './FooterLawfirm.css';
import {NotificationManager,NotificationContainer} from 'react-notifications';

class FooterLawfirm extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            lawinfo:'',
            lwblnk:true,
            lwvalid:true
        }
    }

    getLawVal=(event)=>{
        this.setState({lawinfo:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({lwblnk:true})
            if(event.target.value.length > 300){
                this.setState({lwvalid:false})
            }else{
                this.setState({lwvalid:true})
            }
        }else{
            this.setState({lwblnk:false})
        }
    }

    getVal=()=>{
        let value;
        this.props.data.map(data=>{
            return(
                value=data.lawfirm
            )
        })
        this.setState({lawinfo:value,lwblnk:true,lwvalid:true})
    }

    updateInfo=()=>{
        const {lawinfo}=this.state;
        if(lawinfo.trim() !== "" && lawinfo.length <= 300){
            fetch('https://secure-wave-27786.herokuapp.com/lawfirm/info/update',{
                method:'post',
                headers:{'content-type':'application/json'},
                body:JSON.stringify({
                    id:1,
                    lawfirm:this.state.lawinfo
                })
            })
                .then(response=>response.json())
                .then(data=>{
                    if(data === 'error in update'){
                        NotificationManager.error('Sorry! not updated.')
                    }else{
                        this.props.close();
                        this.setState({lwblnk:true,lwvalid:true})
                        NotificationManager.success('Lawfirm info Updated Successfully!')
                    }
                })
                .catch(console.log)
        }if(lawinfo.trim() === ""){
            this.setState({lwblnk:false})
        }

    }
    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    onEntered={this.getVal}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div className="law-mod-container">
                        <h2>Update Lawfirm Information</h2>
                        <textarea type="text" cols="30" rows="10" placeholder="Lawfirm Information" onChange={this.getLawVal} value={this.state.lawinfo}/>
                        <button onClick={this.updateInfo}>Update</button>
                    </div>
                    <span hidden={this.state.lwblnk} className="error-span">Please enter lawfirm info.</span>
                    <span hidden={this.state.lwvalid} className="error-span">Please enter lawfirm info within 300 characters.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default FooterLawfirm