import React from 'react';
import Modal from 'react-responsive-modal';
import '../AdminPracticeAreaDelete/PracticeDelete.css'
import {NotificationContainer, NotificationManager} from 'react-notifications';

class PostDelete extends React.Component {

    deletePost=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/delete',{
            method:'post',
            headers:{'content-type':'application/json'},
            body:JSON.stringify({
                id:this.props.id
            })
        })
            .then(data=>{
                if(data === 'error in deleted'){
                    NotificationManager.error('Sorry! Post not deleted.')
                }else{
                    this.props.close()
                    NotificationManager.success('Post deleted Successfully!')
                }
            })
            .catch(console.log)
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div className="del-container">
                        Are you sure you want to delete Post Permanentley?
                        <div className="del-btn">
                            <button onClick={this.deletePost}>Yes</button>
                            <button onClick={this.props.close}>No</button>
                        </div>
                    </div>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default PostDelete;