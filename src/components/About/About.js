import React from 'react';
import './About.css';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class About extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            attorney: [],
            loading:true
        }
    }

    componentDidMount() {
        fetch('https://secure-wave-27786.herokuapp.com/attorney/all', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({attorney: data,loading:false})
            })
            .catch(console.log)
    }

    render() {
        return (
            <div className="about-container">
                <div className="about-head">
                    <span>About attorney</span>
                </div>

                {this.state.attorney.map((data,i)=> {
                    return (
                        <div className="about-card" key={i}>
                            <img src={'https://secure-wave-27786.herokuapp.com/' + data.a_image} alt="100x100"/>
                            <h3>{data.a_name}</h3>
                            <p>{data.a_post}</p>
                            <span>{data.a_details}</span>
                        </div>
                    )
                })}
                <div className='sweet-loading-abt'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}

export default About;