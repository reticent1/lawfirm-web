import React from 'react';
import './ClientView.css';
import Modal from 'react-responsive-modal';

class ClientView extends React.Component {

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h4 className="view-head">Client Details</h4>
                        <ul className="ul-element">
                            {this.props.clientId.map(data => {
                                return (
                                    <div>
                                        <h2 className="c-id">#{data.c_id}</h2>
                                        <div key={data.c_id} className="view-container">
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">First Name:</div>
                                                    <div className="view-data">{data.fname}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Last Name:</div>
                                                    <div className="view-data">{data.lname}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Date of Registration:</div>
                                                    <div className="view-data">{data.entry_date.substring(0, 10)}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Date of Birth:</div>
                                                    <div className="view-data">{data.dob}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Gender:</div>
                                                    <div className="view-data">{data.gender}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Contact No.:</div>
                                                    <div className="view-data">{data.mobile}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Email Id:</div>
                                                    <div className="view-data"
                                                         style={{textTransform: 'lowercase'}}>{data.email_id}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Occupation:</div>
                                                    <div className="view-data">{data.occupation}</div>
                                                </li>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </ul>
                    </div>

                </Modal>
            </div>
        );
    }

}

export default ClientView;