import React from 'react';
import './AdminWorkCase.css';
import Modal from 'react-responsive-modal';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import {SingleDatePicker} from 'react-dates';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class AdminWorkCase extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            date: null,
            focused: null,
            date1: null,
            focused1: null,
            c_title: '',
            c_name: '',
            email: '',
            mob: '',
            occupation: '',
            result: '',
            tblnk:true,
            dtblnk:true,
            resblnk:true,
            mobblnk:true,
            mobvalid:true,
            mailblnk:true,
            mailvalid:true,
            occpblnk:true,
            lnblnk:true,
            lnvalid:true
        }
    }

    handleDateChange = (date)=> {
        this.setState({date: date})
        if (date !== null){
            this.setState({dtblnk:true})
        }else if (date === null){
            this.setState({dtblnk:false})
        }
    }

    handleEndDate = (date1)=> {
        this.setState({date1: date1})
    }

    getTitle = (event)=> {
        this.setState({c_title: event.target.value})
        if (event.target.value.trim() !== ""){
            this.setState({tblnk:true})
        }else if (event.target.value.trim() === ""){
            this.setState({tblnk:false})
        }
    }

    getResult = (event)=> {
        this.setState({result: event.target.value})
        if (event.target.value !== ""){
            this.setState({resblnk:true})
        }else if(event.target.value === ""){
            this.setState({resblnk:false})
        }
    }

    getName = (event)=> {
        this.setState({c_name: event.target.value})
        if (event.target.value.trim() === "") {
            this.setState({lnblnk: false, lnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({lnblnk: true})
            if (event.target.value.length < 3) {
                this.setState({lnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({lnvalid: true, lname: event.target.value})
            }
        }
    }

    getEmail = (event)=> {
        this.setState({email: event.target.value})
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (event.target.value.match(mailformat)) {
            this.setState({mailvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({mailblnk: false, mailvalid: true})
        } else {
            this.setState({mailvalid: false, mailblnk: true})
        }
    }

    getMobile = (event)=> {
        const mobValidator = /^\d{9}$/;
        this.setState({mob: event.target.value})
        if (event.target.value.match(mobValidator)) {
            this.setState({
                mobvalid: true,
            })
        } else if (event.target.value.trim() === "") {
            this.setState({mobblnk: false, mobvalid: true})
        } else {
            this.setState({mobvalid: false, mobblnk: true})
        }
    }

    getOccupation = (event)=> {
        this.setState({occupation: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({occpblnk: true})
        } else if (event.target.value.trim() === "") {
            this.setState({occpblnk: false})
        }
    }

    addCase=()=>{
        const {c_title,c_name,email,mob,occupation,result,date}=this.state;
        const mobValidator = /^\d{9}$/;
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if(c_title.trim() !== "" && date !== null && result !== "" && mob.match(mobValidator)
        && email.match(mailformat) && occupation.trim() !== "" && c_name.trim() !== "" && c_name.trim().length >= 3){
            fetch('https://secure-wave-27786.herokuapp.com/case/add',{
                method:'post',
                headers:{'content-type':'application/json'},
                body:JSON.stringify({
                    case_title:this.state.c_title,
                    s_date:this.state.date,
                    e_date:this.state.date1,
                    result:this.state.result,
                    client_name:this.state.c_name,
                    email_id:this.state.email,
                    mobile:this.state.mob,
                    occupation:this.state.occupation,
                    entry_date:new Date()
                })
            })
                .then(response=>response.json())
                .then(data=>{
                    if(data === "case not inserted"){
                        NotificationManager.error("Sorry! Case not Added")
                    }else{
                        NotificationManager.success("Case Added Successfully!")
                        this.props.close()
                    }
                })
                .catch(console.log)
        }
        if (c_title.trim() === ""){
            this.setState({tblnk:false})
        }else if (date === null){
            this.setState({dtblnk:false})
        }else if (result === ""){
            this.setState({resblnk:false})
        }else if(c_name.trim()===""){
            this.setState({lnblnk:false})
        }else if(email.trim() === ""){
            this.setState({mailblnk:false})
        }else if(mob.trim() === ""){
            this.setState({mobblnk:false})
        }else if(occupation.trim() === ""){
            this.setState({occpblnk:false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h1 className="form-head">
                            Case Details
                        </h1>
                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Case Title"
                                   onChange={this.getTitle}/>
                        </div>
                        <div className="case-content">
                            <label className="lable-control lable-control-case">Start:</label>
                            <SingleDatePicker
                                isOutsideRange={() => false}
                                showClearDate={true}
                                small={true}
                                block={true}
                                regular={false}
                                numberOfMonths={1}
                                date={this.state.date}
                                onDateChange={date => this.handleDateChange(date)}
                                focused={this.state.focused}
                                onFocusChange={({focused}) =>
                                    (this.setState({focused}))
                                }
                                displayFormat="DD/MM/YYYY"
                                openDirection="down"
                                hideKeyboardShortcutsPanel={true}
                                showDefaultInputIcon={false}
                                reopenPickerOnClearDate={true}
                                readOnly={true}
                            />
                        </div>
                        <div className="case-content">
                            <label className="lable-control lable-control-case">End :</label>
                            <SingleDatePicker
                                isOutsideRange={() => false}
                                showClearDate={true}
                                small={true}
                                block={true}
                                regular={false}
                                numberOfMonths={1}
                                date={this.state.date1}
                                onDateChange={date => this.handleEndDate(date)}
                                focused={this.state.focused1}
                                onFocusChange={({focused}) =>
                                    (this.setState({focused1: focused}))
                                }
                                displayFormat="DD/MM/YYYY"
                                openDirection="down"
                                hideKeyboardShortcutsPanel={true}
                                showDefaultInputIcon={false}
                                reopenPickerOnClearDate={true}
                                readOnly={true}
                            />
                        </div>
                        <div className="case-content">
                            <select className="case-control" onChange={this.getResult}>
                                <option value=''>Result</option>
                                <option value='live'>Live</option>
                                <option value='won'>Won</option>
                                <option value='lose'>Lose</option>
                            </select>
                        </div>

                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Client Name"
                                   onChange={this.getName}/>
                        </div>
                        <div className="case-content">
                            <input type="email" className="case-control" placeholder="Email Address"
                                   onChange={this.getEmail}/>
                        </div>
                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Mobile No."
                                   onChange={this.getMobile}/>
                        </div>
                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Occupation"
                                   onChange={this.getOccupation}/>
                        </div>
                        <div className="case-content">
                            <button className="appo-btn" onClick={this.addCase}>Add Case</button>
                        </div>
                    </div>
                    <span hidden={this.state.tblnk} className="error-span">Please enter Case Title.</span>
                    <span hidden={this.state.dtblnk} className="error-span">Please enter Start Date.</span>
                    <span hidden={this.state.resblnk} className="error-span">Please select Result.</span>
                    <span hidden={this.state.mobblnk} className="error-span">Please enter Contact No.</span>
                    <span hidden={this.state.mobvalid} className="error-span">Please enter valid Contact No.</span>
                    <span hidden={this.state.mailblnk} className="error-span">Please enter Email Id.</span>
                    <span hidden={this.state.mailvalid} className="error-span">Please enter valid Email Id.</span>
                    <span hidden={this.state.lnblnk} className="error-span">Please enter Client Name</span>
                    <span hidden={this.state.occpblnk} className="error-span">Please enter Occupation of client</span>
                    <span hidden={this.state.lnvalid} className="error-span">Client Name greater than 3 character or more</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }
}

export default AdminWorkCase;