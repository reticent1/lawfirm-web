import React from 'react';
import './CarouselComponent.css'
import './slider-animation.css'
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
import img2 from './images/img6.jpg'
import img3 from './images/img3.jpg'
import img4 from './images/img5.jpg';

class CarouselComponent extends React.Component {
    render() {
        const content = [
            {
                title: `“God works wonders now and then: Behold! A lawyer and an honest man!”`,
                description: '― Benjamin Franklin.',
                button: 'Read More',
                image: `${img4}`,
                clicked:()=>this.props.router('blogs')
            },
            {
                title: `“The rights of every man are diminished when the rights of one man are threatened.”`,
                description: '― John F. Kennedy.',
                button: 'Discover',
                image: `${img3}`,
                clicked:()=>this.props.router('practice')
            },
            {
                title: 'Legal advice from top-rated lawyers',
                description: 'Commercial and Corporate Law Firm in Douala, Cameroon.',
                button: `Book An Appointment`,
                image: `${img2}`,
                clicked: this.props.openModal
            }
        ];

        return (
            <Slider className="slider-wrapper" autoplay="2000ms" touchDisabled="true">
                {content.map((item, index) => (
                    <div
                        key={index}
                        className="slider-content"
                        style={{background: `url('${item.image}') no-repeat center center`}}
                    >
                        <div className="inner">
                            <h1>{item.title}</h1>
                            <p>{item.description}</p>
                            <button className="button-slider" onClick={item.clicked}>{item.button}</button>
                        </div>
                    </div>
                ))}
            </Slider>
        )
    }
}

export default CarouselComponent;