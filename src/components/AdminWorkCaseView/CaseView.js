import React from 'react';
import '../AdminWorkClientView/ClientView.css';
import Modal from 'react-responsive-modal';

class CaseView extends React.Component {

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h4 className="view-head">Case Details</h4>
                        <ul className="ul-element">
                            {this.props.caseId.map(data => {
                                return (
                                    <div>
                                        <h2 className="c-id">#{data.cs_id}</h2>
                                        <div key={data.cs_id} className="view-container">
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Case Title:</div>
                                                    <div className="view-data">{data.case_title}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Start Date:</div>
                                                    <div className="view-data">{data.s_date.substring(0,10)}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">End Date:</div>
                                                    <div className="view-data">{data.e_date === null ? 'not mentioned' :data.e_date.substring(0, 10)}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Result:</div>
                                                    <div className="view-data">{data.result}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Client Name:</div>
                                                    <div className="view-data">{data.client_name}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Contact No.:</div>
                                                    <div className="view-data">{data.mobile}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Email Id:</div>
                                                    <div className="view-data"
                                                         style={{textTransform: 'lowercase'}}>{data.email_id}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Occupation:</div>
                                                    <div className="view-data">{data.occupation}</div>
                                                </li>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </ul>
                    </div>

                </Modal>
            </div>
        );
    }

}

export default CaseView;