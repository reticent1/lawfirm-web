import React from 'react';
import './Work.css'
import clients from './icons/courthouse1.png';
import lawyer from './icons/lawyer.png';
import awards from './icons/police-badge.png';
import WOW from 'wowjs';
import '../css/animate.css';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

const wow = new WOW.WOW();
wow.init()
class Work extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            caseCount: [],
            awardCount: [],
            clientCount: [],
            data: false,
            loading:true
        }
    }

    componentDidMount() {

        fetch('https://secure-wave-27786.herokuapp.com/cases/counts', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({caseCount: data, data: true,loading:false})
            })
            .catch(console.log)

        fetch('https://secure-wave-27786.herokuapp.com/awards/counts', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({awardCount: data,loading:false})
            })
            .catch(console.log)

        fetch('https://secure-wave-27786.herokuapp.com/clients/counts', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({clientCount: data,loading:false})
            })
            .catch(console.log)
    }

    componentWillUnmount(){
        this.setState({
            caseCount: [],
            awardCount: [],
            clientCount: [],
            data: false
        })
    }

    render() {
        const countCase = this.state.caseCount.map(data => {
            return data.count;
        });
        const awardCount = this.state.awardCount.map(data => {
            return data.count;
        });
        const clientCount = this.state.clientCount.map(data => {
            return data.count;
        });
        return (
            <div>
                <div className="card-container wow fadeIn" data-wow-delay="0">
                    <div className="cards">
                        <div className="card-icon">
                            <img src={clients} alt="80*80"/>
                        </div>
                        <div className="card-details">
                            <h1 className="wow fadeInUp " data-wow-delay="30ms">{clientCount}</h1>
                            <p>trusted clients</p>
                        </div>
                    </div>
                    <div className="cards">
                        <div className="card-icon">
                            <img src={lawyer} alt="80*80"/>
                        </div>
                        <div className="card-details">
                            <h1 className="wow fadeInUp" data-wow-delay="60ms">{countCase}</h1>
                            <p>successful cases</p>
                        </div>
                    </div>
                    <div className="cards">
                        <div className="card-icon">
                            <img src={awards} alt="80*80"/>
                        </div>
                        <div className="card-details">
                            <h1 className="wow fadeInUp" data-wow-delay="90ms">{awardCount}</h1>
                            <p>honars & awards</p>
                        </div>
                    </div>
                </div>
                <div className='sweet-loading-wr'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
                {wow.sync()}
            </div>
    );
    }

}

export default Work;