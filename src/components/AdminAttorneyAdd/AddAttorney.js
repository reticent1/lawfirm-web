import React from 'react';
import Modal from 'react-responsive-modal';
import './AddAttorney.css';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class AddAttorney extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            file:null,
            name:'',
            attrPost:'',
            details:'',
            nmblnk:true,
            psblnk:true,
            dtblnk:true,
            dtvalid:true,
            imgblnk:true,
            imgvalid:true
        }
    }

    onImgChoose=(event)=>{
        this.setState({file:event.target.files[0]})
        if (event.target.value.trim() !== ""){
            this.setState({imgblnk:true})
        }else{
            this.setState({imgblnk:false})
        }
    }

    getName=(event)=>{
        this.setState({name:event.target.value});
        if(event.target.value.trim() !== ""){
            this.setState({nmblnk:true})
        }else {
            this.setState({nmblnk:false})
        }
    }

    getDesignition=(event)=>{
        this.setState({attrPost:event.target.value});
        if(event.target.value.trim() !== ""){
            this.setState({psblnk:true})
        }else {
            this.setState({psblnk:false})
        }
    }

    getDetails=(event)=>{
        this.setState({details:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({dtblnk:true})
            if(event.target.value.length >300){
                this.setState({dtvalid:false})
            }else {
                this.setState({dtvalid:true})
            }
        }else {
            this.setState({dtblnk:false})
        }
    }
    addAttorney=()=>{
        const {file,name,attrPost,details}=this.state;
        if(file !== null && file.size <= 3145728 && attrPost.trim() !== "" && details.trim() !== "" && details.length <=300 && name.trim() !== ""){
            const formData = new FormData();
            formData.append('attorneyImage',this.state.file);
            formData.append('name',this.state.name);
            formData.append('designation',this.state.attrPost);
            formData.append('details',this.state.details)
            fetch('https://secure-wave-27786.herokuapp.com/attorney/add',{
                method:'post',
                body:formData
            })
                .then(data=>{
                    if(data === 'not registered'){
                        NotificationManager.error('Sorry! Attorney not added.')
                    }else{
                        this.props.close()
                        this.setState({
                            file:null,
                            name:'',
                            attrPost:'',
                            details:'',
                            nmblnk:true,
                            psblnk:true,
                            dtblnk:true,
                            dtvalid:true,
                            imgblnk:true,
                            imgvalid:true
                        })
                        NotificationManager.success('Attorney added Successfully!')
                    }
                })
                .catch(console.log)
        }if (file === null){
            this.setState({imgblnk:false})
        }else if(name.trim() === ""){
            this.setState({nmblnk:false})
        }else if(attrPost.trim() === ""){
            this.setState({psblnk:false})
        }else if(details.trim() === ""){
            this.setState({dtblnk:false})
        }else if(file.size >= 3145728){
            this.setState({imgvalid:false,imgblnk:true})
        }else if(file.size <= 3145728){
            this.setState({imgvalid:true,imgblnk:true})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h3 className="form-head">Add Attorney</h3>
                        <div className="attorney-content">
                            <input type="file" className="attorney-choose" onChange={this.onImgChoose} accept="image/x-png,image/jpeg"/>
                        </div>
                        <div className="attorney-content">
                            <input type="text" placeholder="Attorney Name" className="attorney-control" onChange={this.getName}/>
                        </div>
                        <div className="attorney-content">
                            <input type="text" placeholder="Attorney Designation" className="attorney-control" onChange={this.getDesignition}/>
                        </div>
                        <div className="attorney-content">
                            <textarea type="text" placeholder="Attorney Education Details" className="attorney-control" cols="30" rows="5" onChange={this.getDetails}/>
                        </div>
                        <div>
                            <button className="appo-btn" onClick={this.addAttorney}>Add</button>
                        </div>
                    </div>
                    <span hidden={this.state.imgblnk} className="error-span">Please upload image.</span>
                    <span hidden={this.state.imgvalid} className="error-span">Please upload image below 3 MB.</span>
                    <span hidden={this.state.nmblnk} className="error-span">Please enter Attorney name.</span>
                    <span hidden={this.state.psblnk} className="error-span">Please enter Attorney Post.</span>
                    <span hidden={this.state.dtblnk} className="error-span">Please enter Attorney work details.</span>
                    <span hidden={this.state.dtvalid} className="error-span">Describe work within 300 characters.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default AddAttorney;