import React from 'react';
import './PicPost.css';
import Modal from 'react-responsive-modal';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class PicPost extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            pic:null,
            picUrl:null,
            imgblnk:true,
            imgvalid:true
        }
    }

    getPic=(event)=>{
        this.setState({
            pic:event.target.files[0],
            picUrl:URL.createObjectURL(event.target.files[0])
        })
        if (event.target.value.trim() !== ""){
            this.setState({imgblnk:true})
        }else{
            this.setState({imgblnk:false})
        }
    }

    changePic=()=>{
        const {pic}=this.state;
        if(pic !== null){
            if(pic.size <= 3145728 ){
                const formData = new FormData();
                formData.append('postImage',this.state.pic);
                formData.append('id',this.props.id);
                fetch('https://secure-wave-27786.herokuapp.com/blogpost/update/image',{
                    method:'post',
                    body:formData
                })
                    .then(data=>{
                        if(data === 'failure'){
                            NotificationManager.error("Sorry! image not update.")
                        }else{
                            this.props.close();
                            this.setState({pic:null,
                                picUrl:null,
                                imgblnk:true,
                                imgvalid:true})
                            NotificationManager.success('Image updated successfully!')
                        }
                    })
                    .catch(console.log)
            }
        }if (pic === null){
            this.setState({imgblnk:false})
        }else if(pic.size >= 3145728){
            this.setState({imgvalid:false,imgblnk:true})
        }else if(pic.size <= 3145728){
            this.setState({imgvalid:true,imgblnk:true})
        }

    }
    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <h3 className="form-head">Change Post Picture</h3>
                    <div className="picture-content">
                        <img src={this.state.picUrl} className="select-img" alt="100x100"/>
                        <input type="file" onChange={this.getPic} accept="image/x-png,image/jpeg" className="picture-control"/>
                    </div>
                    <button className="appo-btn" onClick={this.changePic}>Upload</button>
                    <span hidden={this.state.imgblnk} className="error-span">Please choose image.</span>
                    <span hidden={this.state.imgvalid} className="error-span">Please upload image below 3 MB.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default PicPost;