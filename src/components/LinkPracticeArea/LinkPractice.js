import React from 'react';
import './LinkPractice.css';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;
class LinkPractice extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            laws: [],
            srch:'',
            data:false,
            loading:true
        }
    }

    componentDidMount() {
        fetch('https://secure-wave-27786.herokuapp.com/practice/viewall', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({laws: data,data:true,loading:false})
            })
            .catch(console.log)
    }

    onSearch=(event)=>{
        this.setState({srch:event.target.value})
    }

    render() {

        const filter=this.state.laws.filter(data=>{
            return(
                data.title
                    .toLowerCase().includes(this.state.srch.toLowerCase())
            )
        })
        return (
            <div>
                <div className="l-practice-container wow bounceInLeft" data-wow-delay="0.5s">
                    <div className="l-search-div">
                        <i className="fa fa-search"/> <input type="search" placeholder="Search Law" onChange={this.onSearch} className="l-search-control"/>
                    </div>
                    <div className="l-practice-head">
                        <span>Practice Area</span>
                    </div>
                    {filter.map((data,i)=> {
                        return (
                            <div className="l-practice-cards" key={i}>
                                <div className="l-practice-details">
                                    <h1>{data.title}</h1>
                                    <p>{data.description}</p>
                                </div>
                            </div>
                        )
                    })}
                </div>
                <div className='sweet-loading-l'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }
}

export default LinkPractice;