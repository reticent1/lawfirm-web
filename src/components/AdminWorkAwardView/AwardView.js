import React from 'react';
import '../AdminWorkClientView/ClientView.css'
import Modal from 'react-responsive-modal';

class AwardView extends React.Component {

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h4 className="view-head">Award Details</h4>
                        <ul className="ul-element">
                            {this.props.awardView.map(data => {
                                return (
                                    <div>
                                        <h2 className="c-id">#{data.a_id}</h2>
                                        <div key={data.a_id} className="view-container">
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Award Name:</div>
                                                    <div className="view-data">{data.a_name}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Event Name:</div>
                                                    <div className="view-data">{data.event_name}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Event Date:</div>
                                                    <div className="view-data">{data.a_date.substring(0, 10)}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Received From:</div>
                                                    <div className="view-data">{data.receive_form}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">Entry Date:</div>
                                                    <div className="view-data">{data.entry_date.substring(0,10)}</div>
                                                </li>
                                            </div>
                                            <div className="view-content">
                                                <li>
                                                    <div className="view-header">About:</div>
                                                    <div className="view-data">{data.a_describe}</div>
                                                </li>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </ul>
                    </div>

                </Modal>
            </div>
        );
    }

}

export default AwardView;