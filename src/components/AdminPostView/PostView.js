import React from 'react';
import './PostView.css';
import Modal from 'react-responsive-modal';
import nl2br from 'react-nl2br';

class PostView extends React.Component {

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        {this.props.data.map(data=>{
                            return(
                                <div className="view-post-container" key={data.id}>
                                    <div className="view-post-img" style={{backgroundImage:`url(${'https://secure-wave-27786.herokuapp.com/'+data.p_image})`}}>
                                        <h3 className="form-head">{data.p_title}</h3>
                                        <div>
                                            <div className="view-post-label">Published on: {data.p_date.substring(4,15)}</div>
                                            <div className="view-post-label">By: {data.p_writer}</div>
                                        </div>
                                    </div>
                                    <div className="view-post-details1">
                                        <h1 className="form-head">{data.p_writer} Writes,</h1>
                                        {nl2br(data.p_details)}
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </Modal>
            </div>
        );
    }

}
export default PostView;