import React from 'react';
import Modal from 'react-responsive-modal';
import '../AdminPracticeAreaDelete/PracticeDelete.css'
import {NotificationContainer,NotificationManager} from 'react-notifications';

class PracticeDelete extends React.Component {

    deleteAttorney=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/attorney/delete',{
            method:'post',
            headers:{'content-type':'application/json'},
            body:JSON.stringify({
                id:this.props.id
            })
        })
            .then(data=>{
                if(data === 'error in deleted'){
                    NotificationManager.error('Error in deletion.')
                }else{
                    NotificationManager.success('Attorney deleted successfully!')
                    this.props.close();
                }
            })
            .catch(console.log)
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div className="del-container">
                        Are you sure you want to delete Attorney Permanentley?
                        <div className="del-btn">
                            <button onClick={this.deleteAttorney}>Yes</button>
                            <button onClick={this.props.close}>No</button>
                        </div>
                    </div>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default PracticeDelete;