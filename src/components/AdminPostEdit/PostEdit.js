import React from 'react';
import '../AdminPostAdd/PostAdd.css';
import Modal from 'react-responsive-modal';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class PostEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            p_title:'',
            p_writer:'',
            p_summary:'',
            p_details:'',
            id:'',
            ptblnk: true,
            ptvalid: true,
            pwblnk: true,
            psumblnk: true,
            psumvalid: true,
            pdtblnk: true,
            pdtvalid: true,
        }
    }

    getTitle=(event)=>{
        this.setState({p_title:event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({ptblnk: true})
            if (event.target.value.length > 150) {
                this.setState({ptvalid: false})
            } else {
                this.setState({ptvalid: true})
            }
        } else {
            this.setState({ptblnk: false})
        }
    }
    getWriter=(event)=>{
        this.setState({p_writer:event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({pwblnk: true})
        } else {
            this.setState({pwblnk: false})
        }
    }
    getSummary=(event)=>{
        this.setState({p_summary:event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({psumblnk: true})
            if (event.target.value.length > 300) {
                this.setState({psumvalid: false})
            } else {
                this.setState({psumvalid: true})
            }
        } else {
            this.setState({psumblnk: false})
        }
    }
    getDetails=(event)=>{
        this.setState({p_details:event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({pdtblnk: true})
            if (event.target.value.length > 9999) {
                this.setState({pdtvalid: false})
            } else {
                this.setState({pdtvalid: true})
            }
        } else {
            this.setState({pdtblnk: false})
        }
    }

    getAllValues=()=>{
        let p_title,p_writer,p_summary,p_details,id;
        this.props.data.map(data=>{
            return(
                p_title = data.p_title,
                    p_writer=data.p_writer,
                    p_summary=data.p_summary,
                    p_details=data.p_details,
                    id=data.id
            )
        })
        this.setState({
            p_title:p_title,
            p_writer:p_writer,
            p_summary:p_summary,
            p_details:p_details,
            id:id
        })
    }

    updatePost=()=>{
        const {p_title, p_writer, p_summary, p_details}=this.state;
        if (p_title.trim() !== "" &&
            p_title.length <= 150 &&
            p_summary.length <= 300 &&
            p_details.length <= 9999 &&
            p_writer.trim() !== "" &&
            p_summary.trim() !== "" &&
            p_details.trim() !== ""){
            fetch('https://secure-wave-27786.herokuapp.com/blogpost/update',{
                method:'post',
                headers:{'content-type':'application/json'},
                body:JSON.stringify({
                    id:this.state.id,
                    p_title:this.state.p_title,
                    p_writer:this.state.p_writer,
                    p_summary:this.state.p_summary,
                    p_details:this.state.p_details
                })
            })
                .then(data=> {
                    if (data === 'not update') {
                        NotificationManager.error('Sorry! Post not updated.')
                    } else {
                        this.props.close();
                        this.setState({
                            ptblnk: true,
                            ptvalid: true,
                            pwblnk: true,
                            psumblnk: true,
                            psumvalid: true,
                            pdtblnk: true,
                            pdtvalid: true
                        })
                        NotificationManager.success('Post updated successfully!')
                    }
                })
                .catch(console.log)
        }if (p_title.trim() === "") {
            this.setState({ptblnk: false})
        } else if (p_writer.trim() === "") {
            this.setState({pwblnk: false})
        } else if (p_summary.trim() === "") {
            this.setState({psumblnk: false})
        } else if (p_details.trim() === "") {
            this.setState({pdtblnk: false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    onEntered={this.getAllValues}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <h4 className="form-head">Update Post</h4>
                    <div className="add-post-content">
                        <input type="text" placeholder="Post Title" className="add-post-control" onChange={this.getTitle} value={this.state.p_title}/>
                    </div>
                    <div className="add-post-content">
                        <input type="text" placeholder="Written by" className="add-post-control" onChange={this.getWriter} value={this.state.p_writer}/>
                    </div>
                    <div className="add-post-content">
                        <textarea type="text" placeholder="Short Summary" className="add-post-control" cols="50" rows="5" onChange={this.getSummary} value={this.state.p_summary}/>
                    </div>
                    <div className="add-post-content">
                        <textarea type="text" placeholder="Detail Article" className="add-post-control" cols="100" rows="15" onChange={this.getDetails} value={this.state.p_details}/>
                    </div>
                    <div className="add-post-content">
                        <button className="appo-btn" onClick={this.updatePost}>Update</button>
                    </div>
                    <span hidden={this.state.ptblnk} className="error-span">Please enter post title.</span>
                    <span hidden={this.state.ptvalid} className="error-span">Please enter post title within 150 characters.</span>
                    <span hidden={this.state.pwblnk} className="error-span">Please enter writer name.</span>
                    <span hidden={this.state.psumblnk} className="error-span">Please enter summary.</span>
                    <span hidden={this.state.psumvalid} className="error-span">Please enter summary within 300 characters.</span>
                    <span hidden={this.state.pdtblnk} className="error-span">Please enter details.</span>
                    <span hidden={this.state.pdtvalid} className="error-span">Please enter details within 999 characters.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default PostEdit;