import React from 'react';
import './AdSideDrawer.css';

class AdSideDrawer extends React.Component{
    componentDidMount(){
        window.scrollTo(0,0);
    }
    onLogOut=()=>{
        localStorage.clear();
        this.props.router('app');
    }
    render(){
        let sidedraw = 'ad-side-drawer'
        if(this.props.show){
            sidedraw = 'ad-side-drawer open'
        }
        return(
            <nav className={sidedraw}>
                <ul>
                    <li onClick={()=>this.props.adRouter('appointments')} className={`${this.props.apoint?'active':null}`}>Appointments</li>
                    <li onClick={()=>this.props.adRouter('contact')} className={`${this.props.cont?'active':null}`}>Contact Us</li>
                    <li onClick={()=>this.props.adRouter('workrecord')} className={`${this.props.work?'active':null}`}>Work Record</li>
                    <li onClick={()=>this.props.adRouter('practicearea')} className={`${this.props.pract?'active':null}`}>practice area</li>
                    <li onClick={()=>this.props.adRouter('attorney')} className={`${this.props.attr?'active':null}`}>attorney</li>
                    <li onClick={()=>this.props.adRouter('post')} className={`${this.props.post?'active':null}`}>blog</li>
                    <li onClick={()=>this.props.adRouter('footer')} className={`${this.props.foot?'active':null}`}>footer</li>
                    <li onClick={()=>this.props.adRouter('setting')} className={`${this.props.sett?'active':null}`}>setting</li>
                    <div onClick={this.onLogOut} className="log-div">
                        <i className="fa fa-power-off"/> Logout
                    </div>
                </ul>
            </nav>
        )
    }
}

export default AdSideDrawer;