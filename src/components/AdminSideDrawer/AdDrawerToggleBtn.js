import React from 'react';
import './AdDrawerToggleBtn.css'

class AdDrawerToggleBtn extends React.Component{
    render(props){
        return(
            <button className="ad-toggel-btn" onClick={this.props.clickDrawer}>
                <div className="ad-toggel-btn-line"/>
                <div className="ad-toggel-btn-line"/>
                <div className="ad-toggel-btn-line"/>
            </button>
        )
    }
}
export default AdDrawerToggleBtn;