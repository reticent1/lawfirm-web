import React from 'react';
import './AdminContact.css';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;
class AdminContact extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            contacts: [],
            status:[],
            search: '',
            sortDir: true,
            currPage: 1,
            perPage: 25,
            loading:true,
        }
    }

    componentDidMount() {
        fetch('https://secure-wave-27786.herokuapp.com/contactus/view', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({contacts: data,loading:false})
            })
            .catch(console.log)

        fetch('https://secure-wave-27786.herokuapp.com/contactus/view/status',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({status:data,loading:false})
            })
            .catch(console.log)
    }

    getSearch = (event)=> {
        this.setState({search: event.target.value})
    }

    onSortRecord = (key)=> {
        const sorting = this.state.contacts.sort((a, b) => {
            return this.state.sortDir
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState({
            contacts: sorting, sortDir: !this.state.sortDir
        })
    }

    onSortStatus = (key)=> {
        const sorting = this.state.status.sort((a, b) => {
            return this.state.sortDir
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState({
            status: sorting, sortDir: !this.state.sortDir
        })
    }

    handelPages = (event)=> {
        this.setState({currPage: Number(event.target.value)})
    }

    updateStatus = (id, key)=> {
        fetch('https://secure-wave-27786.herokuapp.com/contactus/update', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                id: id,
                status: 'done'
            })
        })
            .then(response=>response.json())
            .then(data=> {
                if (data === 1) {
                    var x = document.getElementById(key);
                    x.remove(x.selectedIndex);
                }
                console.log(data)
            })
    }

    render() {
        const indexOfLastTodo = this.state.currPage * this.state.perPage;
        const indexOfFirstTodo = indexOfLastTodo - this.state.perPage;
        const currentTodos = this.state.contacts.slice(indexOfFirstTodo, indexOfLastTodo);

        const filtered = currentTodos.filter(data=> {
            return (
                data.fname
                    .concat(' ' + data.lname)
                    .concat(data.id)
                    .toLowerCase().includes(this.state.search.toLowerCase())
            )
        })

        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(this.state.contacts.length / this.state.perPage); i++) {
            pageNumbers.push(i);
        }

        const renderPage = pageNumbers.map((number, key) => {
            return (
                <li
                    key={key}
                    id={key}
                    onClick={this.handelPages}
                    value={number}
                >
                    {number}
                </li>
            );
        })

        const pgHide = this.state.contacts.length < this.state.perPage ? true : false;

        const status = this.state.status.filter(data=> {
            return (
                data.fname
                    .concat(' ' + data.lname)
                    .concat(data.id)
                    .toLowerCase().includes(this.state.search.toLowerCase())
            )
        })
        return (
            <div className="ad-cont-container">
                <div className="con-search-div">
                    <i className="fa fa-search sr-icon"></i>
                    <input type="search" placeholder="Search Contact" className="con-search-bar" onChange={this.getSearch}/>
                </div>
                <div className="ad-cont-content">
                    <h4>Waiting for Response</h4>
                    <table>
                        <tbody>
                        <tr>
                            <th onClick={()=>this.onSortStatus('id')} className="on-hover">Id <i
                                className="fa fa-sort"/></th>
                            <th>F.Name</th>
                            <th>L.Name</th>
                            <th>Contact No.</th>
                            <th>Email Id</th>
                            <th>Message</th>
                            <th>Done</th>
                        </tr>
                        {status.map((data, i)=> {
                                return (
                                    <tr key={data.id} id={i}>
                                        <td>{data.id}</td>
                                        <td>{data.fname}</td>
                                        <td>{data.lname}</td>
                                        <td>{data.mobile}</td>
                                        <td style={{textTransform: 'lowercase'}}>{data.email}</td>
                                        <td>{data.message}</td>
                                        <td><input type="checkbox" id={data.id}
                                                   onClick={()=>this.updateStatus(data.id, i)}/></td>
                                    </tr>
                                )
                        })}
                        </tbody>
                    </table>
                </div>
                <div className="ad-cont-content">
                    <h4 className="h4-head">All</h4>
                    <table>
                        <tbody>
                        <tr>
                            <th onClick={()=>this.onSortRecord('id')} className="on-hover">Id <i
                                className="fa fa-sort"></i></th>
                            <th>F.Name</th>
                            <th>L.Name</th>
                            <th>Contact No.</th>
                            <th>Email Id</th>
                            <th>Message</th>
                            <th>Done</th>
                        </tr>
                        {filtered.map((data, i)=> {
                            return (
                                <tr key={data.id} id={i}>
                                    <td>{data.id}</td>
                                    <td>{data.fname}</td>
                                    <td>{data.lname}</td>
                                    <td>{data.mobile}</td>
                                    <td style={{textTransform: 'lowercase'}}>{data.email}</td>
                                    <td>{data.message}</td>
                                    <td>{data.status === null ?
                                        <input type="checkbox" id={data.id}
                                               onClick={()=>this.updateStatus(data.id, i)}/> :
                                        'Done'}</td>
                                </tr>
                            )
                        })}
                        </tbody>
                    </table>
                    {<div hidden={pgHide}><span className="pagination">{renderPage}</span></div>}
                </div>
                <div className='sweet-loading-cont'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}
export default AdminContact;