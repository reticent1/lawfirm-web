import React from 'react';
import Modal from 'react-responsive-modal';
import './FooterSocial.css';
import {NotificationManager, NotificationContainer} from 'react-notifications';

class FooterSocial extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fb: '',
            tw: '',
            insta: '',
            lin: '',
            fbblnk: true,
            fbval: true,
            twblnk: true,
            twval: true,
            instblnk: true,
            instval: true,
            linblnk: true,
            linval: true,
            id:''
        }
    }

    getFb = (event)=> {
        const webUrl = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/;
        this.setState({fb: event.target.value})
        if (event.target.value.match(webUrl)) {
            this.setState({fbval: true,fbblnk:true})
        } else if (event.target.value.trim() === "") {
            this.setState({fbblnk: false, fbval: true})
        } else {
            this.setState({fbval: false, fbblnk: true})
        }
    }
    getTw = (event)=> {
        const webUrl = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/;
        this.setState({tw: event.target.value})
        if (event.target.value.match(webUrl)) {
            this.setState({twval: true,twblnk:true})
        } else if (event.target.value.trim() === "") {
            this.setState({twblnk: false, twval: true})
        } else {
            this.setState({twval: false, twblnk: true})
        }
    }
    getInsta = (event)=> {
        const webUrl = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/;
        this.setState({insta: event.target.value})
        if (event.target.value.match(webUrl)) {
            this.setState({instval: true,instblnk:true})
        } else if (event.target.value.trim() === "") {
            this.setState({instblnk: false, instval: true})
        } else {
            this.setState({instval: false, instblnk: true})
        }
    }
    getLin = (event)=> {
        const webUrl = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/;
        this.setState({lin: event.target.value})
        if (event.target.value.match(webUrl)) {
            this.setState({linval: true,linblnk:true})
        } else if (event.target.value.trim() === "") {
            this.setState({linblnk: false, linval: true})
        } else {
            this.setState({linval: false, linblnk: true})
        }
    }

    getVal = ()=> {
        let fb, tw, insta, lin,id;
        this.props.data.map(data=> {
            return (
                fb = data.fb,
                    tw = data.tw,
                    insta = data.insta,
                    lin = data.lin,
                    id=data.id
            )
        })
        this.setState({
            fb: fb, tw: tw, insta: insta, lin: lin, fbblnk: true,
            fbval: true,
            twblnk: true,
            twval: true,
            instblnk: true,
            instval: true,
            linblnk: true,
            linval: true,
            id:id
        })
    }
    updateSocial = ()=> {
        const webUrl = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}[.]{0,1}/;
        const {fb, tw, insta, lin,id}=this.state;
        if (fb.match(webUrl) && tw.match(webUrl) && insta.match(webUrl) && lin.match(webUrl)) {
            fetch('https://secure-wave-27786.herokuapp.com/socialmedia/info/update', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    id: id,
                    fb: this.state.fb,
                    tw: this.state.tw,
                    insta: this.state.insta,
                    lin: this.state.lin
                })
            })
                .then(response=>response.json())
                .then(data=> {
                    if (data === 'error in update') {
                        NotificationManager.error('Sorry! Social media not updated.')
                    } else {
                        this.props.close();
                        this.setState({
                            fbblnk: true,
                            fbval: true,
                            twblnk: true,
                            twval: true,
                            instblnk: true,
                            instval: true,
                            linblnk: true,
                            linval: true
                        })
                        NotificationManager.success('Links updated Successfully!')
                    }
                })
                .catch(console.log)
        }
        if (fb.trim() === "") {
            this.setState({fbblnk: false})
        } else if (tw.trim() === "") {
            this.setState({twblnk: false})
        } else if (insta.trim() === "") {
            this.setState({instblnk: false})
        }
        if (lin.trim() === "") {
            this.setState({linblnk: false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    onEntered={this.getVal}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div className="soc-mod-container">
                        <h2>Update Social Links</h2>
                        <input type="text" placeholder="Facebook link" value={this.state.fb} onChange={this.getFb}/>
                        <input type="text" placeholder="Twitter link" value={this.state.tw} onChange={this.getTw}/>
                        <input type="text" placeholder="Instagram link" value={this.state.insta}
                               onChange={this.getInsta}/>
                        <input type="text" placeholder="Linked In link" value={this.state.lin} onChange={this.getLin}/>
                        <button onClick={this.updateSocial}>Update</button>
                    </div>
                    <span hidden={this.state.fbblnk} className="error-span">Please enter Facebook link.</span>
                    <span hidden={this.state.fbval} className="error-span">Please enter valid url for facebook.</span>
                    <span hidden={this.state.twblnk} className="error-span">Please enter Twitter link.</span>
                    <span hidden={this.state.twval} className="error-span">Please enter valid url for twitter.</span>
                    <span hidden={this.state.instblnk} className="error-span">Please enter Instagram link.</span>
                    <span hidden={this.state.instval}
                          className="error-span">Please enter valid url for instagram.</span>
                    <span hidden={this.state.linblnk} className="error-span">Please enter linked in link.</span>
                    <span hidden={this.state.linval} className="error-span">Please enter valid url for linked in.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default FooterSocial;