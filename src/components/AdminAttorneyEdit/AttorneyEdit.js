import React from 'react';
import Modal from 'react-responsive-modal';
import '../AdminAttorneyAdd/AddAttorney.css'
import {NotificationContainer,NotificationManager} from 'react-notifications';


class AttorneyEdit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            attrPost: '',
            details: '',
            a_id:'',
            a_image:'',
            nmblnk:true,
            psblnk:true,
            dtblnk:true,
            dtvalid:true
        }
    }

    getName = (event)=> {
        this.setState({name: event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({nmblnk:true})
        }else {
            this.setState({nmblnk:false})
        }
    }

    getDesignition = (event)=> {
        this.setState({attrPost: event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({psblnk:true})
        }else {
            this.setState({psblnk:false})
        }
    }

    getDetails = (event)=> {
        this.setState({details: event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({dtblnk:true})
            if(event.target.value.length >300){
                this.setState({dtvalid:false})
            }else {
                this.setState({dtvalid:true})
            }
        }else {
            this.setState({dtblnk:false})
        }
    }
    updateAttorney = ()=> {
        const {name,attrPost,details}=this.state;
        if(name.trim() !== "" && attrPost.trim() !== "" && details.trim() !== "" && details.length <=300) {
            fetch('https://secure-wave-27786.herokuapp.com/attorney/update', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    id: this.state.a_id,
                    name: this.state.name,
                    designation: this.state.attrPost,
                    details: this.state.details
                })
            })
                .then(data=>{
                    if(data === 'not update'){
                        NotificationManager.error('Sorry! Attorney not updated.')
                    }else{
                        this.props.close();
                        this.setState({
                            nmblnk:true,
                            psblnk:true,
                            dtblnk:true,
                            dtvalid:true})
                        NotificationManager.success('Attorney updated successfully!')
                    }
                })
                .catch(console.log)
        }if(name.trim() === ""){
            this.setState({nmblnk:false})
        }else if(attrPost.trim() === ""){
            this.setState({psblnk:false})
        }else if(details.trim() === ""){
            this.setState({dtblnk:false})
        }
    }

    getAllValues = ()=> {
        let a_id, a_name, a_post, a_details,a_image;
        this.props.edit.map(data=> {
            return (
                a_id = data.id,
                    a_name = data.a_name,
                    a_post = data.a_post,
                    a_details = data.a_details,
                    a_image=data.a_image
            )
        })
        this.setState({
            a_id:a_id,
            name:a_name,
            attrPost:a_post,
            details:a_details,
            a_image:a_image
        })
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    onEntered={this.getAllValues}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h3 className="form-head">Update Attorney</h3>
                        <div className="attorney-content">
                            <img src={'https://secure-wave-27786.herokuapp.com/'+this.state.a_image} alt="200x200" className="select-img"/>
                        </div>
                        <div className="attorney-content">
                            <input type="text" placeholder="Attorney Name" className="attorney-control" value={this.state.name}
                                   onChange={this.getName}/>
                        </div>
                        <div className="attorney-content">
                            <input type="text" placeholder="Attorney Designation" className="attorney-control" value={this.state.attrPost}
                                   onChange={this.getDesignition}/>
                        </div>
                        <div className="attorney-content">
                            <textarea type="text" placeholder="Attorney Education Details" className="attorney-control" value={this.state.details}
                                      cols="30" rows="5" onChange={this.getDetails}/>
                        </div>
                        <div>
                            <button className="appo-btn" onClick={this.updateAttorney}>Update</button>
                        </div>
                    </div>
                    <span hidden={this.state.nmblnk} className="error-span">Please enter Attorney name.</span>
                    <span hidden={this.state.psblnk} className="error-span">Please enter Attorney Post.</span>
                    <span hidden={this.state.dtblnk} className="error-span">Please enter Attorney work details.</span>
                    <span hidden={this.state.dtvalid} className="error-span">Describe work within 300 characters.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default AttorneyEdit;