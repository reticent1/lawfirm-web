import React from 'react';
import './Blog.css';
import PostView from '../AdminPostView/PostView';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class Blog extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            addPost:false,
            posts:[],
            views:[],
            viewPost:false,
            id:'',
            data:false,
            loading:true
        }
    }

    fetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/all',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({posts:data,data:true,loading:false})
            })
            .catch(console.log)
    }

    componentDidMount(){
        this.fetcher();
    }

    onViewPostOpen=(key)=>{
        this.setState({viewPost:true})
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/view',{
            method:'post',
            headers:{'content-type':'application/json'},
            body:JSON.stringify({
                id:key
            })
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({views:data})
            })
            .catch(console.log)
    }

    onViewPostClose=()=>{
        this.setState({viewPost:false})
        this.fetcher();
    }


    render() {
        let hide;
        if(this.state.posts.length === 3 || this.state.posts.length < 3){
            hide=true;
        }else {
            hide=false
        }
        return (
            <div className="post-container">
                <div className="blog-head">
                    <span>Blog</span>
                </div>
                {this.state.posts.slice(0,3).map(data =>{
                    return(
                        <div className="post-content" key={data.id}>
                            <div className="post-head" style={{backgroundImage: `url(${'https://secure-wave-27786.herokuapp.com/'+data.p_image})`}}>
                                <h3>{data.p_title}</h3>
                                <div>
                                    <div className="post-label">Published on: {data.p_date.substring(4,15)}</div>
                                    <div className="post-label">By: {data.p_writer}</div>
                                </div>
                            </div>
                            <div className="post-describe">
                                <h4>Summary</h4>
                                <p>
                                    {data.p_summary}
                                </p>
                            </div>
                            <div className="post-icons" onClick={()=>this.onViewPostOpen(data.id)}>
                                <i className="fa fa-eye"/> Read
                            </div>
                        </div>
                    )
                })}
                <div className="l-explore-btn" hidden={hide}>
                    <span onClick={()=>this.props.router('blogs')}>explore More <i className="fa fa-long-arrow-right"/></span>
                </div>
                <PostView
                    open={this.state.viewPost}
                    close={this.onViewPostClose}
                    data={this.state.views}
                />
                <div className='sweet-loading-bg'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}
export default Blog;