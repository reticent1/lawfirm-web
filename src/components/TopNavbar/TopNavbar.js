import React from 'react';
import './TopNavbar.css'
import DraweToggelButton from '../SideDrawer/DrawerToggelButton'

class TopNavbar extends React.Component {

    componentDidMount(){
        window.scrollTo(0,0);
    }

    render() {
        return (
            <header className="toolbar">
                <nav className="toolbar-navigation">
                    <div className="toolbar-logo">
                        <button className='a'>SANG<span style={{color: '#706E6E'}}>GOR<span
                        onClick={this.props.openLog}>G</span>E</span></button></div>
                    <div className="spacer"/>
                    <div className="toolbar-navigation-items">
                        <ul>
                            <li onClick={()=>this.props.router('app')} className={`${this.props.home?'active':null}`}>home</li>
                            <li onClick={()=>this.props.router('practice')} className={`${this.props.pr?'active':null}`}>practice area</li>
                            <li onClick={()=>this.props.router('blogs')} className={`${this.props.blg?'active':null}`}>blog</li>
                            <li onClick={()=>this.props.router('about')} className={`${this.props.abt?'active':null}`}>about</li>
                            <li onClick={()=>this.props.router('contactus')} className={`${this.props.cntc?'active':null}`}>contact</li>
                            <li className="btn-map">
                                <button className="aa">
                                    <span onClick={this.props.openModal}>
                                        make an appointment
                                    </span>
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <DraweToggelButton clickDrawer={this.props.drawerHandler}/>
                    </div>
                </nav>
            </header>
        );
    }
}

export default TopNavbar;