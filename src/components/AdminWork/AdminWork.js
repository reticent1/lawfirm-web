import React from 'react';
import './AdminWork.css';
import Awards from './workicons/trophy.png'
import Client from './workicons/students.png';
import Success from  './workicons/target.png';
import ClientView from '../AdminWorkClientView/ClientView'
import ClientUpdate from '../AdminWorkClientUpdate/ClientUpdate'
import CaseView from '../AdminWorkCaseView/CaseView';
import CaseUpdate from '../AdminWorkCaseUpdate/CaseUpdate';
import AwardView from '../AdminWorkAwardView/AwardView';
import AwardUpdate from '../AdminWorkAwardUpdate/AwardUpdate';
import AdminWorkClient from '../AdminWorkClient/AdminWorkClient';
import AdminWorkCase from '../AdminWorkCase/AdminWorkCase';
import AdminWorkAward from '../AdminWorkAward/AdminWorkAward';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class AdminWork extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            clients: [],
            searching: '',
            sortDir: true,
            sortCase:true,
            sortAward:true,
            viewOpen: false,
            cView: [],
            updateOpen: false,
            cases: [],
            searchCase: '',
            caseCount:[],
            caseView:false,
            viewCase:[],
            caseUpdate:false,
            awards:[],
            searchAward:'',
            awardView:false,
            awardUpdate:false,
            viewAward:[],
            open:false,
            caseOpen:false,
            awardOpen:false,
            loading:true
        }
    }

    onClientModalOpen=()=>{
        this.setState({open:true})
    }

    onClientModalClose=()=>{
        this.setState({open:false})
        fetch('https://secure-wave-27786.herokuapp.com/clients', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response => response.json())
            .then(data=> {
                this.setState({clients: data})
            })
            .catch(console.log)
    }

    onCaseAddModalOpen = ()=> {
        this.setState({caseOpen: true})
    }

    onCaseAddModalClose = ()=> {
        this.setState({caseOpen: false})
        fetch('https://secure-wave-27786.herokuapp.com/cases', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({cases: data})
            })
            .catch(console.log)

        fetch('https://secure-wave-27786.herokuapp.com/cases/counts',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({caseCount:data})
            })
            .catch(console.log)
    }

    onAwardModalOpen = ()=> {
        this.setState({awardOpen: true})
    }

    onAwardModalClose = ()=> {
        this.setState({awardOpen: false})
        fetch('https://secure-wave-27786.herokuapp.com/awards/',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({awards:data})
            })
            .catch(console.log)
    }

    onViewModalOpen = (key)=> {
        this.setState({viewOpen: true})
        fetch('https://secure-wave-27786.herokuapp.com/clients/view', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                c_id: key
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({cView: data})
                console.log(this.state.cupdate)
            })
            .catch(console.log)
    }

    onViewModalClose = ()=> {
        this.setState({viewOpen: false})
    }

    onUpdateModalOpen = (key)=> {
        this.setState({updateOpen: true})
        fetch('https://secure-wave-27786.herokuapp.com/clients/view', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                c_id: key
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({cView: data})
            })
            .catch(console.log)
    }

    onUpdateModalClose = ()=> {
        this.setState({updateOpen: false})
    }

    onCaseModalOpen=(key)=>{
        this.setState({caseView:true})
        fetch('https://secure-wave-27786.herokuapp.com/cases/view', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                cs_id: key
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({viewCase: data})
            })
            .catch(console.log)
    }

    onCaseModalClose=()=>{
        this.setState({caseView:false})
    }

    onCaseUpdateOpen=(key)=>{
        this.setState({caseUpdate:true})
        fetch('https://secure-wave-27786.herokuapp.com/cases/view', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                cs_id: key
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({viewCase: data})
            })
            .catch(console.log)
    }

    onCaseUpdateClose=()=>{
        this.setState({caseUpdate:false})
    }

    onAwardViewOpen=(key)=>{
        this.setState({awardView:true})
        fetch('https://secure-wave-27786.herokuapp.com/awards/view',{
            method:'post',
            headers:{'content-type':'application/json'},
            body:JSON.stringify({
                a_id:key
            })
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({viewAward:data})
            })
            .catch(console.log)
    }

    onAwardViewClose=()=>{
        this.setState({awardView:false})
    }

    onAwardUpdateOpen=(key)=>{
        this.setState({awardUpdate:true})
        fetch('https://secure-wave-27786.herokuapp.com/awards/view',{
            method:'post',
            headers:{'content-type':'application/json'},
            body:JSON.stringify({
                a_id:key
            })
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({viewAward:data})
            })
            .catch(console.log)
    }

    onAwardUpdateClose=()=>{
        this.setState({awardUpdate:false})
    }

    fetchClient = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/clients', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response => response.json())
            .then(data=> {
                this.setState({clients: data,loading:false})
            })
            .catch(console.log)
    }

    fetchCases = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/cases', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({cases: data,loading:false})
            })
            .catch(console.log)
    }

    componentDidMount() {
        this.fetchClient();
        this.fetchCases();
        fetch('https://secure-wave-27786.herokuapp.com/cases/counts',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({caseCount:data})
            })
            .catch(console.log)

        fetch('https://secure-wave-27786.herokuapp.com/awards/',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({awards:data,loading:false})
            })
            .catch(console.log)
    }

    getSearch = (event)=> {
        this.setState({searching: event.target.value})
    }

    sortClient = (key)=> {
        const sorting = this.state.clients.sort((a, b)=> {
            return this.state.sortDir
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState(
            {clients: sorting, sortDir: !this.state.sortDir}
        )
    }

    getSearchCase = (event)=> {
        this.setState({searchCase: event.target.value})
    }

    sortCases = (key)=> {
        const sorting = this.state.cases.sort((a, b)=> {
            return this.state.sortCase
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState(
            {cases: sorting, sortCase: !this.state.sortCase}
        )
    }

    getSearchAward = (event)=>{
        this.setState({searchAward:event.target.value})
    }

    sortAwards = (key)=> {
        const sorting = this.state.awards.sort((a, b)=> {
            return this.state.sortAward
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState(
            {awards: sorting, sortAward: !this.state.sortAward}
        )
    }

    render() {
        const filter = this.state.clients.filter(data=> {
            return (
                data.fname
                    .concat(' ' + data.lname)
                    .concat(data.c_id)
                    .toLowerCase().includes(this.state.searching.toLowerCase())
            )
        })

        const filterCase = this.state.cases.filter(data=> {
            return (
                data.case_title
                    .concat(data.cs_id)
                    .concat(data.result)
                    .toLowerCase().includes(this.state.searchCase.toLowerCase())
            )
        })

        const filterAward = this.state.awards.filter(data=>{
            return(
                data.a_name
                    .concat(data.a_id)
                    .toLowerCase().includes(this.state.searchAward.toLowerCase())
            )
        })

        const countCase = this.state.caseCount.map(data => {
            return data.count;
        });

        return (
            <div>
                <div className="work-container">
                    <div className="work-content">
                        <div className="work-img">
                            <img src={Client} alt="50x50"/>
                        </div>
                        <div className="work-info">
                            <p>{this.state.clients.length}</p>
                            <h5>Trusted Clients</h5>
                            <button onClick={this.onClientModalOpen}><i className="fa fa-user-plus"> </i> Add Client</button>
                        </div>
                    </div>
                    <div className="work-content">
                        <div className="work-img">
                            <img src={Success} alt="50x50"/>
                        </div>
                        <div className="work-info">
                            <p>{countCase}</p>
                            <h5>Successful Cases</h5>
                            <button onClick={this.onCaseAddModalOpen}><i className="fa fa-book"> </i> Add Case</button>
                        </div>
                    </div>
                    <div className="work-content">
                        <div className="work-img">
                            <img src={Awards} alt="50x50"/>
                        </div>
                        <div className="work-info">
                            <p>{this.state.awards.length}</p>
                            <h5>Honars & Awards</h5>
                            <button onClick={this.onAwardModalOpen}><i className="fa fa-shield"> </i> Add Awards
                            </button>
                        </div>
                    </div>
                    <div className='sweet-loading-work'>
                        <PropagateLoader
                            className={override}
                            sizeUnit={"px"}
                            size={15}
                            color={'#958C8C'}
                            loading={this.state.loading}
                        />
                    </div>
                </div>
                <div className="table-container">
                    <div className="client-table">
                        <div className="work-head">
                            <h4><i className="fa fa-user-circle-o"> </i> Clients</h4>
                            <i className="fa fa-search"></i>
                            <input type="search" placeholder="Search Name" onChange={this.getSearch}/>
                        </div>
                        <table>
                            <tbody>
                            <tr>
                                <th onClick={()=>this.sortClient('c_id')} className="th-hover">ID <i
                                    className="fa fa-sort"></i></th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Action</th>
                            </tr>
                            {filter.map((client, key)=> {
                                return (
                                    <tr key={key} id={client.c_id}>
                                        <td>{client.c_id}</td>
                                        <td>{client.fname}</td>
                                        <td>{client.lname}</td>
                                        <td>
                                            <div className="btn-div">
                                                <button onClick={()=>this.onViewModalOpen(client.c_id)}><i
                                                    className="fa fa-eye"></i></button>
                                                <button onClick={()=>this.onUpdateModalOpen(client.c_id)}><i
                                                    className="fa fa-pencil"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                            }
                            </tbody>
                        </table>
                    </div>
                    <div className="client-table">
                        <div className="work-head">
                            <h4><i className="fa fa-balance-scale" aria-hidden="true"></i> Cases</h4>
                            <i className="fa fa-search"></i>
                            <input type="search" placeholder="Search Case" onChange={this.getSearchCase}/>
                        </div>
                        <table>
                            <tbody>
                            <tr>
                                <th onClick={()=>this.sortCases('cs_id')} className="th-hover">ID <i
                                    className="fa fa-sort"></i></th>
                                <th>Title</th>
                                <th>Result</th>
                                <th>Action</th>
                            </tr>
                            {filterCase.map((cases, key)=> {
                                return (
                                    <tr key={key}>
                                        <td>{cases.cs_id}</td>
                                        <td>{cases.case_title}</td>
                                        <td>{cases.result}</td>
                                        <td>
                                            <div className="btn-div">
                                                <button onClick={()=>this.onCaseModalOpen(cases.cs_id)}><i
                                                    className="fa fa-eye"></i></button>
                                                <button onClick={()=>this.onCaseUpdateOpen(cases.cs_id)}><i
                                                    className="fa fa-pencil"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>
                    <div className="client-table">
                        <div className="work-head">
                            <h4><i className="fa fa-star-half-o" aria-hidden="true"></i> Awards</h4>
                            <i className="fa fa-search"></i>
                            <input type="search" placeholder="Search Award" onChange={this.getSearchAward}/>
                        </div>
                        <table>
                            <tbody>
                            <tr>
                                <th onClick={()=>this.sortAwards('a_id')} className="th-hover">ID <i
                                    className="fa fa-sort"></i></th>
                                <th>Award Name</th>
                                <th>Action</th>
                            </tr>
                            {filterAward.map(data=>{
                              return(
                                  <tr key={data.a_id}>
                                      <td>{data.a_id}</td>
                                      <td>{data.a_name}</td>
                                      <td>
                                          <div className="btn-div">
                                              <button onClick={()=>this.onAwardViewOpen(data.a_id)}><i
                                                  className="fa fa-eye"></i></button>
                                              <button onClick={()=>this.onAwardUpdateOpen(data.a_id)}><i
                                                  className="fa fa-pencil"></i></button>
                                          </div>
                                      </td>
                                  </tr>
                              )
                            })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
                <ClientView
                    open={this.state.viewOpen}
                    close={this.onViewModalClose}
                    clientId={this.state.cView}
                />
                <ClientUpdate
                    open={this.state.updateOpen}
                    close={this.onUpdateModalClose}
                    update={this.state.cView}
                />
                <CaseView
                    open={this.state.caseView}
                    close={this.onCaseModalClose}
                    caseId={this.state.viewCase}
                />
                <CaseUpdate
                    open={this.state.caseUpdate}
                    close={this.onCaseUpdateClose}
                    caseUpdate={this.state.viewCase}
                />
                <AwardView
                    open={this.state.awardView}
                    close={this.onAwardViewClose}
                    awardView={this.state.viewAward}
                />
                <AwardUpdate
                    open={this.state.awardUpdate}
                    close={this.onAwardUpdateClose}
                    awardUpdate={this.state.viewAward}
                />
                <AdminWorkClient
                    open={this.state.open}
                    close={this.onClientModalClose}
                />
                <AdminWorkCase
                    open={this.state.caseOpen}
                    close={this.onCaseAddModalClose}
                />
                <AdminWorkAward
                    open={this.state.awardOpen}
                    close={this.onAwardModalClose}
                />
            </div>
        );
    }

}

export default AdminWork;