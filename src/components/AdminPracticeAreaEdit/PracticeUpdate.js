import React from 'react';
import Modal from 'react-responsive-modal';
import '../AdminPracticeAreaAdd/PracticeAdd.css';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class PracticeUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            law:'',
            describe:'',
            id:'',
            lawblnk:true,
            dsblnk:true,
            dsvalid:true
        }
    }

    getTitle=(event)=>{
        this.setState({law:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({lawblnk:true})
        }else{
            this.setState({lawblnk:false})
        }
    }

    getDescribe=(event)=>{
        this.setState({describe:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({dsblnk:true})
            if(event.target.value.length>300){
                this.setState({dsvalid:false})
            }else{
                this.setState({dsvalid:true})
            }
        }else {
            this.setState({dsblnk:false})
        }
    }

    getAllValues=()=>{
        let title,describe,id;
        this.props.view.map(data=>{
            return(
                id=data.id,
                title=data.title,
                    describe=data.description
            )
        })
        this.setState({
            id:id,
            law:title,
            describe:describe
        })
    }

    updatePracticeArea=()=>{
        const {law,describe}=this.state;
        if (law.trim() !== "" && describe.trim() !== "" && describe.length <=300) {
            fetch('https://secure-wave-27786.herokuapp.com/practice/update', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    id: this.state.id,
                    title: this.state.law,
                    describe: this.state.describe
                })
            })
                .then(response => response.json())
                .then(data=>{
                    if(data === 'existed'){
                        NotificationManager.error('Practice Law already existed.');
                    }else if(data === 'not updated'){
                        NotificationManager.error('Sorry! Practice Law not updated.');
                    }else{
                        this.props.close()
                        NotificationManager.success('Practice Law updated successfully!')
                    }
                })
                .catch(console.log)
        }if (law.trim() === ""){
            this.setState({lawblnk:false})
        }else if(describe.trim() === ""){
            this.setState({dsblnk:false})
        }else if(describe.length > 300){
            this.setState({dsvalid:false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onEntered={this.getAllValues}
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h4 className="form-head">Update {this.state.law}</h4>
                        <div className="pr-add-content">
                            <input type="text" placeholder="Law Rule title" className="pr-add-control" value={this.state.law} onChange={this.getTitle}/>
                        </div>
                        <div className="pr-add-content">
                            <textarea cols="30" rows="5" type="text" placeholder="describe in short" value={this.state.describe} className="pr-add-control" onChange={this.getDescribe}/>
                        </div>
                        <div>
                            <button className="appo-btn" onClick={this.updatePracticeArea}>Update</button>
                        </div>
                    </div>
                    <span hidden={this.state.lawblnk} className="error-span">Please enter Law Rule</span>
                    <span hidden={this.state.dsblnk} className="error-span">Please enter Law description</span>
                    <span hidden={this.state.dsvalid} className="error-span">Please describe within 300 characters.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default PracticeUpdate;