import React from 'react';
import '../AdminWorkCase/AdminWorkCase.css';
import Modal from 'react-responsive-modal';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class CaseUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            s_date: '',
            e_date: '',
            c_title: '',
            c_name: '',
            email: '',
            mob: '',
            occupation: '',
            result: '',
            c_id: '',
            tblnk: true,
            dtblnk: true,
            resblnk: true,
            mobblnk: true,
            mobvalid: true,
            mailblnk: true,
            mailvalid: true,
            occpblnk: true,
            lnblnk: true,
            lnvalid: true
        }
    }

    handleStartDate = (event)=> {
        this.setState({s_date: event.target.value})
        if (event.target.value.trim() !== '') {
            this.setState({dtblnk: true})
        } else if (event.target.value.trim() === '') {
            this.setState({dtblnk: false})
        }
    }

    handleEndDate = (event)=> {
        this.setState({e_date: event.target.value})
    }

    getTitle = (event)=> {
        this.setState({c_title: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({tblnk: true})
        } else if (event.target.value.trim() === "") {
            this.setState({tblnk: false})
        }
    }

    getResult = (event)=> {
        this.setState({result: event.target.value})
        if (event.target.value !== "") {
            this.setState({resblnk: true})
        } else if (event.target.value === "") {
            this.setState({resblnk: false})
        }
    }

    getName = (event)=> {
        this.setState({c_name: event.target.value})
        if (event.target.value.trim() === "") {
            this.setState({lnblnk: false, lnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({lnblnk: true})
            if (event.target.value.length < 3) {
                this.setState({lnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({lnvalid: true, lname: event.target.value})
            }
        }
    }

    getEmail = (event)=> {
        this.setState({email: event.target.value})
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (event.target.value.match(mailformat)) {
            this.setState({mailvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({mailblnk: false, mailvalid: true})
        } else {
            this.setState({mailvalid: false, mailblnk: true})
        }
    }

    getMobile = (event)=> {
        const mobValidator = /^\d{9}$/;
        this.setState({mob: event.target.value})
        if (event.target.value.match(mobValidator)) {
            this.setState({
                mobvalid: true,
            })
        } else if (event.target.value.trim() === "") {
            this.setState({mobblnk: false, mobvalid: true})
        } else {
            this.setState({mobvalid: false, mobblnk: true})
        }
    }

    getOccupation = (event)=> {
        this.setState({occupation: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({occpblnk: true})
        } else if (event.target.value.trim() === "") {
            this.setState({occpblnk: false})
        }
    }

    getPropsValues = ()=> {
        let cs_id, c_title, s_date, e_date, result, c_name, email, mobile, occupation;
        this.props.caseUpdate.map(data=> {
            return (
                cs_id = data.cs_id,
                    c_title = data.case_title,
                    s_date = data.s_date.substring(0, 10),
                    e_date = data.e_date === null ? null : data.e_date.substring(0, 10),
                    result = data.result,
                    c_name = data.client_name,
                    email = data.email_id,
                    mobile = data.mobile,
                    occupation = data.occupation
            )
        })
        this.setState({
            c_id: cs_id,
            c_title: c_title,
            s_date: s_date,
            e_date: e_date,
            c_name: c_name,
            email: email,
            mob: mobile,
            result: result,
            occupation: occupation
        })
    }

    updateCase = ()=> {
        const {c_title, c_name, email, mob, occupation, result, date}=this.state;
        const mobValidator = /^\d{9}$/;
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (c_title.trim() !== "" && date !== '' && result !== "" && mob.match(mobValidator)
            && email.match(mailformat) && occupation.trim() !== "" && c_name.trim() !== "" && c_name.trim().length >= 3) {
            fetch('https://secure-wave-27786.herokuapp.com/case/update', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    cs_id: this.state.c_id,
                    case_title: this.state.c_title,
                    s_date: this.state.s_date,
                    e_date: this.state.e_date,
                    result: this.state.result,
                    client_name: this.state.c_name,
                    email_id: this.state.email,
                    mobile: this.state.mob,
                    occupation: this.state.occupation
                })
            })
                .then(response=>response.json())
                .then(data=>{
                    if(data === 'Not Updated'){
                        NotificationManager.error("Sorry! Case not updated.")
                    }else{
                        NotificationManager.success("Case updated Successfully!")
                        this.props.close()
                    }
                })
                .catch(console.log)
        }if (c_title.trim() === ""){
            this.setState({tblnk:false})
        }else if (date === ''){
            this.setState({dtblnk:false})
        }else if (result === ""){
            this.setState({resblnk:false})
        }else if(c_name.trim()===""){
            this.setState({lnblnk:false})
        }else if(email.trim() === ""){
            this.setState({mailblnk:false})
        }else if(mob.trim() === ""){
            this.setState({mobblnk:false})
        }else if(occupation.trim() === ""){
            this.setState({occpblnk:false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onEntered={this.getPropsValues}
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h1 className="form-head">
                            Case Update
                        </h1>
                        <h3>#{this.state.c_id}</h3>
                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Case Title"
                                   value={this.state.c_title}
                                   onChange={this.getTitle}/>
                        </div>
                        <div className="case-content">
                            <label className="lable-control lable-control-case">Start:</label>
                            <input type="date" className="case-control" value={this.state.s_date}
                                   onChange={this.handleStartDate}/>
                        </div>
                        <div className="case-content">
                            <label className="lable-control lable-control-case">End :</label>
                            <input type="date" className="case-control" value={this.state.e_date}
                                   onChange={this.handleEndDate}/>
                        </div>
                        <div className="case-content">
                            <select className="case-control" onChange={this.getResult} value={this.state.result}>
                                <option value=''>Result</option>
                                <option value='live'>Live</option>
                                <option value='won'>Won</option>
                                <option value='lose'>Lose</option>
                            </select>
                        </div>

                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Client Name"
                                   value={this.state.c_name}
                                   onChange={this.getName}/>
                        </div>
                        <div className="case-content">
                            <input type="email" className="case-control" placeholder="Email Address"
                                   value={this.state.email}
                                   onChange={this.getEmail}/>
                        </div>
                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Mobile No." value={this.state.mob}
                                   onChange={this.getMobile}/>
                        </div>
                        <div className="case-content">
                            <input type="text" className="case-control" placeholder="Occupation"
                                   value={this.state.occupation}
                                   onChange={this.getOccupation}/>
                        </div>
                        <div className="case-content">
                            <button className="appo-btn" onClick={this.updateCase}>Update</button>
                        </div>
                    </div>
                    <span hidden={this.state.tblnk} className="error-span">Please enter Case Title.</span>
                    <span hidden={this.state.dtblnk} className="error-span">Please enter Start Date.</span>
                    <span hidden={this.state.resblnk} className="error-span">Please select Result.</span>
                    <span hidden={this.state.mobblnk} className="error-span">Please enter Contact No.</span>
                    <span hidden={this.state.mobvalid} className="error-span">Please enter valid Contact No.</span>
                    <span hidden={this.state.mailblnk} className="error-span">Please enter Email Id.</span>
                    <span hidden={this.state.mailvalid} className="error-span">Please enter valid Email Id.</span>
                    <span hidden={this.state.lnblnk} className="error-span">Please enter Client Name</span>
                    <span hidden={this.state.occpblnk} className="error-span">Please enter Occupation of client</span>
                    <span hidden={this.state.lnvalid} className="error-span">Client Name greater than 3 character or more</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }
}

export default CaseUpdate;