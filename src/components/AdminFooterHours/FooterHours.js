import React from 'react';
import './FooterHours.css';
import Modal from 'react-responsive-modal';
import {NotificationManager, NotificationContainer} from 'react-notifications';

class FooterHours extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            mtot: '',
            sat: '',
            fri: '',
            mtotvl: true,
            stvl: true,
            frvl: true
        }
    }

    getMtoThr = (event)=> {
        this.setState({mtot: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({mtotvl: true})
        } else {
            this.setState({mtotvl: false})
        }
    }
    getSat = (event)=> {
        this.setState({sat: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({stvl: true})
        } else {
            this.setState({stvl: false})
        }
    }
    getFri = (event)=> {
        this.setState({fri: event.target.value})
        if (event.target.value.trim() !== "") {
            this.setState({frvl: true})
        } else {
            this.setState({frvl: false})
        }
    }
    getVal = ()=> {
        let mtot, fri, sat;
        this.props.data.map(data=> {
            return (
                mtot = data.montothru,
                    fri = data.fri,
                    sat = data.sat
            )
        })
        this.setState({
            mtot: mtot,
            fri: fri,
            sat: sat,
            mtotvl: true,
            stvl: true,
            frvl: true
        })
    }
    updateHours = ()=> {
        const {mtot, fri, sat}=this.state;
        if (mtot.trim() !== "" && fri.trim() !== "" && sat.trim() !== "") {
            fetch('https://secure-wave-27786.herokuapp.com/openhours/info/update', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    id: 1,
                    montothru: this.state.mtot,
                    fri: this.state.fri,
                    sat: this.state.sat
                })
            })
                .then(response=>response.json())
                .then(data=> {
                    if (data === 'error in update') {
                        NotificationManager.error('Sorry! Not updated')
                    } else {
                        this.props.close()
                        this.setState({
                            mtotvl: true,
                            stvl: true,
                            frvl: true
                        })
                        NotificationManager.success('Working hour updated Successfully!')
                    }
                })
                .catch(console.log)
        }
        if (mtot.trim() === "") {
            this.setState({mtotvl: false})
        } else if (fri.trim() === "") {
            this.setState({frvl: false})
        } else if (sat.trim() === "") {
            this.setState({stvl: false})
        }

    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    onEntered={this.getVal}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div className="hour-mod-container">
                        <h2>Update Open Hours</h2>
                        <input type="text" placeholder="Monday To Thrusday Time slot" value={this.state.mtot}
                               onChange={this.getMtoThr}/>
                        <input type="text" placeholder="Friday Time slot" value={this.state.fri}
                               onChange={this.getFri}/>
                        <input type="text" placeholder="Saturday Time slot" value={this.state.sat}
                               onChange={this.getSat}/>
                        <button onClick={this.updateHours}>Update</button>
                    </div>
                    <span hidden={this.state.mtotvl} className="error-span">Please enter Monday to Thrusday Time.</span>
                    <span hidden={this.state.frvl} className="error-span">Please enter Friday Time.</span>
                    <span hidden={this.state.stvl} className="error-span">Please enter Saturday Time.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default FooterHours;