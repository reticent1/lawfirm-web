import React from 'react';
import './ManageAppointments.css';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class ManageAppointments extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            appoints: [],
            todays: [],
            confirms: [],
            pendings: [],
            slot: null,
            changeDate: null,
            searchField: '',
            hideAll: true,
            hideToday: true,
            hideCnf: true,
            hidePend: true,
            currentPage: 1,
            perPage: 25,
            sortDir: true,
            loading:true,
        }

    }

    handelPages = (event)=> {
        this.setState({currentPage: Number(event.target.value)})
    }

    allAppoint = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/appointments', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response => response.json())
            .then(data => {
                this.setState({appoints: data, hideAll: false, hideToday: true, hideCnf: true, hidePend: true,isActive:false,loading:false})
            })
            .catch(console.log)
    }

    selectSlot = (e)=> {
        this.setState({slot: e.target.value})
    }

    selectDate = (e)=> {
        this.setState({changeDate: e.target.value})
    }

    updateAppoint = (id, akey)=> {
        if (this.state.changeDate === null) {
            NotificationManager.info('Please select Date and Slot');
        } else if (this.state.slot === null) {
            NotificationManager.info('Please select Date and Slot');
        } else {
            fetch('https://secure-wave-27786.herokuapp.com/update/appointment', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    ap_id: id,
                    c_date: this.state.changeDate,
                    ap_slot: this.state.slot
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data === 1) {
                        NotificationManager.success('Appointments booked successfully.');
                        var x = document.getElementById(akey);
                        x.remove(x.selectedIndex);
                        this.setState({changeDate:null,slot:null})
                    }
                })
                .catch(error=> {
                    NotificationManager.error('Record not updated.')
                })
        }

    }

    searchAppointment = (event)=> {
        this.setState({searchField: event.target.value});
    }

    todaysAppoint = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/appointments/today', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response => response.json())
            .then(data => {
                this.setState({todays: data, hideAll: true, hideToday: false, hideCnf: true, hidePend: true})
                console.log(this.state.todays)
            })
            .catch(console.log)
    }

    confrimAppoint = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/appointments/confirm', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data => {
                this.setState({confirms: data, hideAll: true, hideToday: true, hideCnf: false, hidePend: true})
            })
            .catch(console.log);
    }

    pendingAppoint = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/appointments/waiting', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data => {
                this.setState({pendings: data, hideAll: true, hideToday: true, hideCnf: true, hidePend: false})
            })
            .catch(console.log);
    }

    componentDidMount() {
        this.todaysAppoint();
        this.confrimAppoint();
        this.pendingAppoint();
        this.allAppoint();
    }

    sortField = (key)=> {
        const sorting = this.state.appoints.sort((a, b) => {
            return this.state.sortDir
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState({
            appoints: sorting, sortDir: !this.state.sortDir
        })
    }

    sortTodayField = (key)=> {
        const sorting = this.state.todays.sort((a, b) => {
            return this.state.sortDir
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState({
            todays: sorting, sortDir: !this.state.sortDir
        })
    }

    sortConfirmField = (key)=> {
        const sorting = this.state.confirms.sort((a, b) => {
            return this.state.sortDir
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState({
            confirms: sorting, sortDir: !this.state.sortDir
        })
    }

    sortWaitField = (key)=> {
        const sorting = this.state.pendings.sort((a, b) => {
            return this.state.sortDir
                ? a[key] - b[key]
                : b[key] - a[key]
        });
        this.setState({
            pendings: sorting, sortDir: !this.state.sortDir
        })
    }

    render() {

        const todayFilter = this.state.todays.filter(today => {
            return (
                today.fname
                    .concat(' ' + today.lname)
                    .concat(today.appoint_id)
                    .toLowerCase().includes(this.state.searchField.toLowerCase())
            )
        })

        const confirmFilter = this.state.confirms.filter(confirm=> {
            return (
                confirm.fname
                    .concat(' ' + confirm.lname)
                    .concat(confirm.appoint_id)
                    .toLowerCase().includes(this.state.searchField.toLowerCase())
            )
        })

        const pendingFilter = this.state.pendings.filter(pends=> {
            return (
                pends.fname
                    .concat(' ' + pends.lname)
                    .concat(pends.appoint_id)
                    .toLowerCase().includes(this.state.searchField.toLowerCase())
            )
        })

        const indexOfLastTodo = this.state.currentPage * this.state.perPage;
        const indexOfFirstTodo = indexOfLastTodo - this.state.perPage;
        const currentTodos = this.state.appoints.slice(indexOfFirstTodo, indexOfLastTodo);

        const filtered = currentTodos.filter(appoints=> {
            return (
                appoints.fname
                    .concat(' ' + appoints.lname)
                    .concat(appoints.appoint_id)
                    .toLowerCase().includes(this.state.searchField.toLowerCase())
            )
        })

        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(this.state.appoints.length / this.state.perPage); i++) {
            pageNumbers.push(i);
        }

        const renderPage = pageNumbers.map((number, key) => {
            return (
                <li
                    key={key}
                    id={key}
                    onClick={this.handelPages}
                    value={number}
                >
                    {number}
                </li>
            );
        })

        const pgHide = this.state.appoints.length < this.state.perPage ? true : false;
        return (
            <div className="appoint-container">
                <div className="appoint-nav">
                    <ul className="ul">
                        <li>
                            <button onClick={this.allAppoint}><span><i className="fa fa-globe"/> </span>
                                All
                            </button>
                        </li>
                        <li>
                            <button onClick={this.todaysAppoint}>
                                <span><i className="fa fa-calendar"/> </span>
                                Today's
                                <span style={{color: 'green', fontSize: '16px'}}> ({this.state.todays.length})</span>
                            </button>
                        </li>
                        <li>
                            <button onClick={this.confrimAppoint}>
                                <span><i className="fa fa-check-square-o"/> </span>
                                Confirmed
                                <span style={{color: 'orange', fontSize: '16px'}}> ({this.state.confirms.length})</span>
                            </button>
                        </li>
                        <li>
                            <button onClick={this.pendingAppoint}>
                                <span><i className="fa fa-pause"/> </span>
                                Pending
                                <span style={{color: 'red', fontSize: '16px'}}> ({this.state.pendings.length})</span>
                            </button>
                        </li>
                    </ul>
                    <ul className="srch-app">
                        <li>
                            <i className="fa fa-search"/>
                            <input type="search" className="srch-app-text" placeholder="search appointments"
                                   onChange={this.searchAppointment}/>
                        </li>
                    </ul>
                </div>

                <table hidden={this.state.hideAll}>
                    <tbody>
                    <tr>
                        <th onClick={()=> this.sortField('appoint_id')} className="th-hover">
                            Id <i className="fa fa-sort"/>
                        </th>
                        <th>F.Name</th>
                        <th>L.Name</th>
                        <th>Contact</th>
                        <th>Email Id</th>
                        <th>DOA</th>
                        <th>Message</th>
                        <th>Slot</th>
                        <th>Status</th>
                        <th>Update</th>
                    </tr>
                    {filtered.map((datas, key) => {
                        return (
                            <tr id={key} key={key}>
                                <td className="class-center">{datas.appoint_id}</td>
                                <td>{datas.fname}</td>
                                <td>{datas.lname}</td>
                                <td>{datas.contact_no}</td>
                                <td>{datas.email_id}</td>
                                <td>
                                    <div>
                                        <small>
                                            Request date: {datas.ap_date.substring(0, 10)}
                                        </small>
                                    </div>
                                    <div>
                                        Edit: <input type="date" className="date-input" onChange={this.selectDate}/>
                                    </div>
                                </td>
                                <td>{datas.message}</td>
                                <td><div>
                                    <small>
                                        Slot: {datas.time_slot === null ? 'NA' : datas.time_slot}
                                    </small>
                                    </div>
                                    <select className="select-input" onChange={this.selectSlot}>
                                    <option value="">Select</option>
                                    <option value="9am - 10am">09AM-10AM</option>
                                    <option value="10am - 11am">10AM-11AM</option>
                                    <option value="11am - 12pm">11AM-12PM</option>
                                    <option value="2pm - 3pm">02PM-03PM</option>
                                </select></td>
                                <td>{datas.confirm_appoint}</td>
                                <td>
                                    <button className="btn-confirm" id={key}
                                            onClick={()=>this.updateAppoint(datas.appoint_id, key)}>Edit
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
                {<div hidden={this.state.hideAll || pgHide}><span className="pagination">{renderPage}</span></div>}

                <table hidden={this.state.hideToday}>
                    <tbody>
                    <tr>
                        <th onClick={()=> this.sortTodayField('appoint_id')} className="th-hover">
                            Id <i className="fa fa-sort"/>
                        </th>
                        <th>F.Name</th>
                        <th>L.Name</th>
                        <th>Contact</th>
                        <th>Email Id</th>
                        <th>DOA</th>
                        <th>Message</th>
                        <th>Slot</th>
                        <th>Status</th>
                    </tr>
                    {
                        todayFilter.map((tdata, tkey)=> {
                            return (
                                <tr key={tkey} id={tkey}>
                                    <td className="class-center">{tdata.appoint_id}</td>
                                    <td>{tdata.fname}</td>
                                    <td>{tdata.lname}</td>
                                    <td>{tdata.contact_no}</td>
                                    <td>{tdata.email_id}</td>
                                    <td>{tdata.ap_date.substring(0, 10)}</td>
                                    <td>{tdata.message}</td>
                                    <td>{tdata.time_slot}</td>
                                    <td>{tdata.confirm_appoint}</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>

                <table hidden={this.state.hideCnf}>
                    <tbody>
                    <tr>
                        <th onClick={()=> this.sortConfirmField('appoint_id')} className="th-hover">
                            Id <i className="fa fa-sort"/>
                        </th>
                        <th>F.Name</th>
                        <th>L.Name</th>
                        <th>Contact</th>
                        <th>Email Id</th>
                        <th>DOA</th>
                        <th>Message</th>
                        <th>Slot</th>
                        <th>Status</th>
                    </tr>
                    {
                        confirmFilter.map((cdata, ckey)=> {
                            return (
                                <tr key={ckey} id={ckey}>
                                    <td className="class-center">{cdata.appoint_id}</td>
                                    <td>{cdata.fname}</td>
                                    <td>{cdata.lname}</td>
                                    <td>{cdata.contact_no}</td>
                                    <td>{cdata.email_id}</td>
                                    <td>{cdata.ap_date.substring(0, 10)}</td>
                                    <td>{cdata.message}</td>
                                    <td>{cdata.time_slot}</td>
                                    <td>{cdata.confirm_appoint}</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>

                <table hidden={this.state.hidePend}>
                    <tbody>
                    <tr>
                        <th onClick={()=> this.sortWaitField('appoint_id')} className="th-hover">
                            Id <i className="fa fa-sort"/>
                        </th>
                        <th>F.Name</th>
                        <th>L.Name</th>
                        <th>Contact</th>
                        <th>Email Id</th>
                        <th>DOA</th>
                        <th>Message</th>
                        <th>Slot</th>
                        <th>Update</th>
                    </tr>
                    {pendingFilter.map((datas, key) => {
                        return (
                            <tr id={key} key={key}>
                                <td className="class-center">{datas.appoint_id}</td>
                                <td>{datas.fname}</td>
                                <td>{datas.lname}</td>
                                <td>{datas.contact_no}</td>
                                <td>{datas.email_id}</td>
                                <td>
                                    <div>
                                        <small>
                                            Request date: {datas.ap_date.substring(0, 10)}
                                        </small>
                                    </div>
                                    <div>
                                        Edit: <input type="date" className="date-input" onChange={this.selectDate}/>
                                    </div>
                                </td>
                                <td>{datas.message}</td>
                                <td><select className="select-input" onChange={this.selectSlot}>
                                    <option value="">Select</option>
                                    <option value="9am - 10am">09AM-10AM</option>
                                    <option value="10am - 11am">10AM-11AM</option>
                                    <option value="11am - 12pm">11AM-12PM</option>
                                    <option value="2pm - 3pm">02PM-03PM</option>
                                </select></td>
                                <td>
                                    <button className="btn-confirm" id={key}
                                            onClick={()=>this.updateAppoint(datas.appoint_id, key)}>Edit
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
                <NotificationContainer/>
                <div className='sweet-loading'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }
}
export default ManageAppointments;