import React from 'react'
import './BackDrop.css'

class BackDrop extends React.Component{
    render(){
        return(
            <div className="backdrop" onClick={this.props.closeBackDrop}/>
        )
    }
}

export default BackDrop