import React from 'react';
import './AdminLogin.css';
import Modal from 'react-responsive-modal';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class AdminLogin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            passKey: '',
            hide: false,
            mail: '',
            pass: '',
            pkblank: true,
            pkvalid: true,
            unblank: true,
            unvalid: true,
            psblank: true
        }
    }

    passVal = (e)=> {
        this.setState({passKey: e.target.value})
        if (e.target.value.trim() === "") {
            this.setState({pkblank: false, pkvalid: true})
        }
        if (e.target.value.trim() !== "") {
            this.setState({pkblank: true, pkvalid: true})
        }
    }

    checkPassKey = ()=> {
        if (this.state.passKey !== "") {
            fetch('https://secure-wave-27786.herokuapp.com/adminlogin/passkey', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    passkey: this.state.passKey
                })
            })
                .then(response=>response.json())
                .then(data=> {
                    if (data === 'success') {
                        this.setState({hide: true})
                        NotificationManager.success('Passkey Matched')
                    } else {
                        this.setState({pkblank: true, pkvalid: false})
                    }
                })
                .catch(console.log)
        }
        if (this.state.passKey === ""){
            this.setState({pkblank: false, pkvalid: true})
        }
    }

    getMail = (event)=> {
        this.setState({mail: event.target.value})
        if (event.target.value.trim() === "") {
            this.setState({unblank: false,unvalid:true})
        } else {
            this.setState({unblank: true})
        }
    }

    getPass = (event)=> {
        this.setState({pass: event.target.value})
        if (event.target.value.trim() === "") {
            this.setState({psblank: false,unvalid:true})
        } else {
            this.setState({psblank: true})
        }
    }

    onLogin = ()=> {
        const {mail, pass}=this.state;
        if (mail !== "" && pass !== "") {
            fetch('https://secure-wave-27786.herokuapp.com/adminlogin/login', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    uname: this.state.mail,
                    password: this.state.pass
                })
            })
                .then(response=>response.json())
                .then(data=> {
                        if (data === 'password not matched' || data === 'Invalid credential' || data === 'failure') {
                            this.setState({unvalid: false})
                        } else{
                            this.setState({unvalid: true})
                            this.props.data(data);
                            this.props.onRouter('adminpanel');
                            this.props.closeLog();
                        }
                    }
                )
                .catch(console.log)
        }
        if (mail === ""){
            this.setState({unblank: false,unvalid:true})
        }
       else if (pass === ""){
            this.setState({psblank: false,unvalid:true})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.closeLog}
                    open={this.props.openLog}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                    animationDuration={1000}
                >
                    <div hidden={this.state.hide}>
                        <span className="form-head">Please Enter Passkey.</span>
                        <div className="form2-content">
                            <input type="password" className="form2-control" placeholder="PassKey"
                                   onChange={this.passVal}/>
                        </div>
                        <div className="form2-content">
                            <button onClick={this.checkPassKey} className="appo-btn">Proceed</button>
                        </div>
                    </div>
                    <div hidden={!this.state.hide}>
                        <span className="form-head">
                            Welcome to Login
                        </span>
                        <div className="form2-content">
                            <input type="text" className="form2-control" placeholder="Username"
                                   onChange={this.getMail}/>
                        </div>
                        <div>
                            <input type="password" className="form2-control" placeholder="Password"
                                   onChange={this.getPass}/>
                        </div>
                        <div className="form2-content">
                            <button className="appo-btn" onClick={this.onLogin}>Login</button>
                            <button className="appo-btn" style={{float: 'right', background: 'orange'}}>Reset</button>
                        </div>
                    </div>
                    <span hidden={this.state.pkblank} className="error-span">Please enter PASSKEY</span>
                    <span hidden={this.state.pkvalid} className="error-span">Invalid Key</span>
                    <span hidden={this.state.unblank} className="error-span">Please Enter Username</span>
                    <span hidden={this.state.psblank} className="error-span">Please Enter Password</span>
                    <span hidden={this.state.unvalid} className="error-span">Invalid Credentials</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}

export default AdminLogin;