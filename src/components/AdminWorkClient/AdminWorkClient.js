import React from 'react';
import './AdminWorkClient.css';
import Modal from 'react-responsive-modal';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class AdminWorkClient extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fname:'',
            lname:'',
            dob:null,
            gender:'',
            mob:'',
            email:'',
            occupation:'',
            fnblnk:true,
            fnvalid:true,
            lnblnk:true,
            lnvalid:true,
            dobblnk:true,
            genblnk:true,
            mobblnk:true,
            mobvalid:true,
            mailblnk:true,
            mailvalid:true,
            occpblnk:true
        }
    }

    getFname=(event)=>{
        if (event.target.value.trim() === "") {
            this.setState({fnblnk: false, fnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({fnblnk: true})
            if (event.target.value.length < 3) {
                this.setState({fnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({fnvalid: true, fname: event.target.value})
            }
        }
    }

    getLname=(event)=>{
        if (event.target.value.trim() === "") {
            this.setState({lnblnk: false, lnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({lnblnk: true})
            if (event.target.value.length < 3) {
                this.setState({lnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({lnvalid: true, lname: event.target.value})
            }
        }
    }

    getDob=(event)=>{
        if(event.target.value.trim() !== ""){
            this.setState({dob:event.target.value,dobblnk:true})
        }else if (event.target.value.trim() === ""){
            this.setState({dobblnk:false})
        }
    }

    getGender=(event)=>{
        if (event.target.value !== ""){
            this.setState({gender:event.target.value,genblnk:true})
        }else if(event.target.value === ""){
            this.setState({genblnk:false})
        }
    }

    getMob=(event)=>{
        const mobValidator = /^\d{9}$/;
        this.setState({mob: event.target.value})
        if (event.target.value.match(mobValidator)) {
            this.setState({
                mobvalid: true,
            })
        } else if (event.target.value.trim() === "") {
            this.setState({mobblnk: false, mobvalid: true})
        } else {
            this.setState({mobvalid: false, mobblnk: true})
        }
    }

    getEmail=(event)=>{
        this.setState({email:event.target.value})
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (event.target.value.match(mailformat)) {
            this.setState({mailvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({mailblnk: false, mailvalid: true})
        } else {
            this.setState({mailvalid: false, mailblnk: true})
        }
    }

    getOccupation=(event)=>{
        if (event.target.value.trim() !== ""){
            this.setState({occupation:event.target.value,occpblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({occpblnk:false})
        }
    }

    addClient=()=>{
        const {fname,lname,dob,gender,mob,email,occupation}=this.state;
        const mobValidator = /^\d{9}$/;
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        if (fname.trim() !== "" &&
            fname.length >= 3 &&
            lname.trim() !== "" &&
            lname.length >= 3 &&
            dob !== null &&
            gender !== "" &&
            mob.match(mobValidator) &&
            email.match(mailformat) && occupation.trim() !== ""){
            fetch('https://secure-wave-27786.herokuapp.com/client/add',{
                method:'post',
                headers:{'content-type':'application/json'},
                body:JSON.stringify({
                    fname:this.state.fname,
                    lname:this.state.lname,
                    gender:this.state.gender,
                    mobile:this.state.mob,
                    email_id:this.state.email,
                    occupation:this.state.occupation,
                    entry_date:new Date(),
                    dob:this.state.dob
                })
            })
                .then(response => response.json())
                .then(data=>{
                    if (data === 'error in insert'){
                        NotificationManager.error('Sorry! Client not added');
                    }else if(data === 'existed'){
                        NotificationManager.info('Email or Mobile number already present');
                    }
                    else {
                        NotificationManager.success('Client Added Successfully');
                        this.props.close()
                    }
                })
                .catch(console.log)
        }
        if (fname.trim() === ""){
            this.setState({fnblnk:false})
        }else if (lname.trim() === ""){
            this.setState({lnblnk:false})
        }else if (dob === null){
            this.setState({dobblnk:false})
        }else if (gender === ""){
            this.setState({genblnk:false})
        }else if (mob.trim() === ""){
            this.setState({mobblnk:false})
        }else if (email.trim() === ""){
            this.setState({mailblnk:false})
        }else if (occupation.trim() === ""){
            this.setState({occpblnk:false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                    animationDuration={1000}
                >
                    <div>
                        <h1 className="form-head">
                            Client Details
                        </h1>
                        <div className="client-content">
                            <input type="text" className="client-name" placeholder="First Name" onChange={this.getFname}/>
                            <input type="text" className="client-name" placeholder="Last Name" onChange={this.getLname}/>
                        </div>
                        <div className="client-content" style={{display:'inline-block', marginTop:'0'}}>
                            <label className="lable-control">Date of Birth:</label>
                            <input type="date" className="date-control" onChange={this.getDob}/>
                        </div>
                        <div className="client-content" style={{display:'inline-block', margin:'0'}}>
                            <label className="lable-control" >Gender:</label>
                            <select className="select-control" onChange={this.getGender}>
                                <option value="">Select</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                        <div className="client-content">
                            <input type="text" className="client-control" placeholder="Mobile Number"
                                   onChange={this.getMob}/>
                        </div>
                        <div className="client-content">
                            <input type="email" className="client-control" placeholder="Email Address"
                                   onChange={this.getEmail}/>
                        </div>
                        <div className="client-content">
                            <input type="text" className="client-control" placeholder="Occupation"
                                   onChange={this.getOccupation}/>
                        </div>
                        <div className="client-content">
                            <button className="appo-btn" onClick={this.addClient}>Add Client</button>
                        </div>
                    </div>
                    <span hidden={this.state.fnblnk} className="error-span">Please enter First Name</span>
                    <span hidden={this.state.fnvalid} className="error-span">First Name must be 3 character or more</span>
                    <span hidden={this.state.lnblnk} className="error-span">Please enter Last Name</span>
                    <span hidden={this.state.lnvalid} className="error-span">Last name must be 3 character or more</span>
                    <span hidden={this.state.dobblnk} className="error-span">Please enter DOB.</span>
                    <span hidden={this.state.genblnk} className="error-span">Please Select Gender.</span>
                    <span hidden={this.state.mobblnk} className="error-span">Please enter contact no.</span>
                    <span hidden={this.state.mobvalid} className="error-span">Please enter valid contact no.</span>
                    <span hidden={this.state.mailblnk} className="error-span">Please enter Email Id.</span>
                    <span hidden={this.state.mailvalid} className="error-span">Please enter valid Email Id.</span>
                    <span hidden={this.state.occpblnk} className="error-span">Please enter Occupation.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }
}

export default AdminWorkClient;