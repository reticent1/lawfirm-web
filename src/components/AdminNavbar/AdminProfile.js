import React from 'react';
import './AdminProfile.css'
import AdDrawerToggleBtn from '../AdminSideDrawer/AdDrawerToggleBtn';

class AdminProfile extends React.Component {

    onLogout=()=>{
        localStorage.clear();
        this.props.router('app');
    }
    render() {
        return (
            <div>
                <header className="ad-toolbar">
                    <nav className="ad-toolbar-navigation">
                        <div className="ad-toolbar-logo">Hello, {this.props.data.fname} {this.props.data.lname}</div>
                        <div className="spacer"/>
                        <div className="ad-toolbar-navigation-items">
                            <ul>
                                <li onClick={this.onLogout}><i className="fa fa-power-off" ></i>logout</li>
                            </ul>
                        </div>
                        <div>
                            <AdDrawerToggleBtn clickDrawer={this.props.drawerHandler}/>
                        </div>
                    </nav>
                </header>
            </div>

        );
    }
}

export default AdminProfile;