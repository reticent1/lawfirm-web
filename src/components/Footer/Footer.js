import React from 'react';
import './Footer.css';
// import 'font-awesome/css/font-awesome.min.css';

class Footer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lawfirm: [],
            contact: [],
            hours: [],
            social: [],
            data:false
        }
    }

    lawfirmFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/lawfirm/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({lawfirm: data})
            })
            .catch(console.log)
    }

    contactFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/contactinfo/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({contact: data,data:true})
            })
            .catch(console.log)
    }

    hourFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/openhours/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({hours: data})
            })
            .catch(console.log)
    }

    socialFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/socialmedia/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({social: data})
            })
            .catch(console.log)
    }
    componentDidMount() {
        this.lawfirmFetcher();
        this.contactFetcher();
        this.hourFetcher();
        this.socialFetcher();
    }

    render() {
        return (
            <div className="footer-container">
                <div className="footer-content">
                    <span>Law Firm</span>
                    <hr/>
                    {this.state.lawfirm.map((data,i)=>{
                        return(
                            <div key={i}>
                                <p>{data.lawfirm}</p>
                            </div>
                        )
                    })}
                </div>
                <div className="footer-content">
                    <span>Contact Information</span>
                    <hr/>
                    {this.state.contact.map((data,i)=>{
                        return(
                            <div key={i}>
                                <p>{data.address}<br/>
                                    {data.mobile}<br/>
                                    {data.email}</p>
                            </div>
                        )
                    })}
                </div>
                <div className="footer-content">
                    <span>Opening Hours</span>
                    <hr/>
                    {this.state.hours.map((data,i)=>{
                        return(
                            <div key={i}>
                                <p>Mon - Thu: {data.montothru}<br/>
                                    Fri {data.fri}<br/>
                                    Sat {data.sat}</p>
                            </div>
                        )
                    })}
                </div>
                {this.state.social.map((data,i)=>{
                    return(
                        <div className="social-media" key={i}>
                            <ul>
                                <li>
                                    <a href={data.fb} target="blank">
                                        <i className="fa fa-facebook-official fa-2x"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href={data.tw} target="blank">
                                        <i className="fa fa-twitter fa-2x"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href={data.insta} target="blank">
                                        <i className="fa fa-instagram fa-2x"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href={data.lin} target="blank">
                                        <i className="fa fa-linkedin fa-2x"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    )
                })}
                <div className="copy-content">
                    <p>Copyright © 2018 Lawfirm. All Right Reserved. Designed and Developed by
                        <a href="https://www.facebook.com/kamlesh.mohane.10" target="blank"> Kamlesh D. Mohane</a></p>
                </div>
            </div>
        );
    }
}
export default Footer;