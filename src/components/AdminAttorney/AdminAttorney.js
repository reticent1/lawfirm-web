import React from 'react';
import './AdminAttorney.css';
import AddAttorney from '../AdminAttorneyAdd/AddAttorney'
import AttorneyEdit from '../AdminAttorneyEdit/AttorneyEdit'
import AttorneyPicChange from '../AdminAttorneyChangePic/AttorneyPicChange'
import AttorneyDelete from '../AdminAttorneyDelete/AttorneyDelete'
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class AdminAttorney extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addAttr: false,
            attorney: [],
            searchAttr: '',
            edit: [],
            editAttr: false,
            picOpen: false,
            passId: null,
            delOpen: false,
            delId: null,
            updater:1,
            loading:true,
        }
    }

    fetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/attorney/all', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({attorney: data,loading:false})
            })
            .catch(console.log)
    }

    componentDidMount() {
        this.fetcher()
    }

    onAddOpen = ()=> {
        this.setState({addAttr: true})
    }

    onAddClose = ()=> {
        this.setState({addAttr: false},()=>{console.log(this.state.addAttr)});
        this.fetcher()
    }

    onEditOpen = (key)=> {
        this.setState({editAttr: true})
        fetch('https://secure-wave-27786.herokuapp.com/attorney/view', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                id: key
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({edit: data})
                console.log(this.state.edit)
            })
            .catch(console.log)
    }

    onEditClose = ()=> {
        this.setState({editAttr: false})
    }

    onPicOpen = (key)=> {
        this.setState({picOpen: true, passId: key})
    }

    onPicClose = ()=> {
        this.setState({picOpen: false})
        this.fetcher()
    }

    onDelOpen = (key)=> {
        this.setState({delOpen: true, delId: key})
    }

    onDelClose = ()=> {
        this.setState({delOpen: false,updater:this.state.updater+1},()=>{console.log(this.state.addAttr)});
        this.fetcher()
    }

    onSearchAttr = (event)=> {
        this.setState({searchAttr: event.target.value})
    }

    render() {
        const filter = this.state.attorney.filter(data=> {
            return (
                data.a_name
                    .concat(data.a_post)
                    .toLowerCase()
                    .includes(this.state.searchAttr.toLowerCase())
            )
        })
        return (
            <div>
                <div className="attorney-container">
                    <div className="attorney-search">
                        <input type="search" placeholder="Search Attorney" className="search-control"
                               onChange={this.onSearchAttr}/><i className="fa fa-search"></i>
                    </div>
                    {filter.map(data=> {
                        return (
                            <div className="attorney-card" key={data.id}>
                                <img src={'https://secure-wave-27786.herokuapp.com/' + data.a_image} alt="100x100"/>
                                <h3>{data.a_name}</h3>
                                <p>{data.a_post}</p>
                                <span>{data.a_details}</span>
                                <div className="attorney-icons">
                                    <i className="fa fa-pencil-square-o" onClick={()=>this.onEditOpen(data.id)}></i>
                                    <i className="fa fa-picture-o" onClick={()=>this.onPicOpen(data.id)}></i>
                                    <i className="fa fa-trash-o" onClick={()=>this.onDelOpen(data.id)}></i>
                                </div>
                            </div>
                        )
                    })}
                    <div className="attorney-card">
                        <button className="attorney-btn" onClick={this.onAddOpen}>
                            <i className="fa fa-plus-square-o"></i>
                            <p>Add Attorney</p>
                        </button>
                    </div>
                    <AddAttorney
                        open={this.state.addAttr}
                        close={this.onAddClose}
                    />
                    <AttorneyEdit
                        open={this.state.editAttr}
                        close={this.onEditClose}
                        edit={this.state.edit}
                    />
                    <AttorneyPicChange
                        open={this.state.picOpen}
                        close={this.onPicClose}
                        id={this.state.passId}
                    />
                    <AttorneyDelete
                        open={this.state.delOpen}
                        close={this.onDelClose}
                        id={this.state.delId}
                    />
                </div>
                <div className='sweet-loading-attr'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>

        );
    }

}
export default AdminAttorney;