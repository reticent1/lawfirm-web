import React from 'react';
import './AdminFooter.css';
import FooterLawfirm from '../AdminFooterLawfirm/FooterLawfirm';
import FooterContact from '../AdminFooterContact/FooterContact';
import FooterHours from '../AdminFooterHours/FooterHours';
import FooterSocial from '../AdminFooterSocial/FooterSocial';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class AdminFooter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lawfirm: [],
            contact: [],
            hours: [],
            social: [],
            lawOpen:false,
            conOpen:false,
            hrOpen:false,
            socOpen:false,
            loading:true
        }
    }

    lawfirmFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/lawfirm/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({lawfirm: data,loading:false})
            })
            .catch(console.log)
    }

    contactFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/contactinfo/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({contact: data,loading:false})
            })
            .catch(console.log)
    }

    hourFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/openhours/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({hours: data,loading:false})
            })
            .catch(console.log)
    }

    socialFetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/socialmedia/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({social: data,loading:false})
            })
            .catch(console.log)
    }
    componentDidMount() {
        this.lawfirmFetcher();
        this.contactFetcher();
        this.hourFetcher();
        this.socialFetcher();
    }

    onLawModalOpen=()=>{
        this.setState({lawOpen:true})
    }
    onLawModalClose=()=>{
        this.setState({lawOpen:false})
        this.lawfirmFetcher();
    }
    onConModalOpen=()=>{
        this.setState({conOpen:true})
    }
    onConModalClose=()=>{
        this.setState({conOpen:false})
        this.contactFetcher();
    }
    onHrModalOpen=()=>{
        this.setState({hrOpen:true})
    }
    onHrModalClose=()=>{
        this.setState({hrOpen:false})
        this.hourFetcher();
    }
    onSocModalOpen=()=>{
        this.setState({socOpen:true})
    }
    onSocModalClose=()=>{
        this.setState({socOpen:false})
        this.socialFetcher();
    }
    render() {
        return (
            <div className="ad-foot-container">
                {this.state.lawfirm.map(data=> {
                    return (
                        <div className="ad-foot-info" key={data.id}>
                            <h1>Lawfirm Information</h1>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-balance-scale"></i> Lawfrim</div>
                                <div className="add-data">
                                    {data.lawfirm}
                                </div>
                            </div>
                            <div className="ad-foot-info-content">
                                <button className="ad-foot-edit" onClick={this.onLawModalOpen}><i className="fa fa-pencil-square-o"></i> Edit</button>
                            </div>
                        </div>
                    )
                })}

                {this.state.contact.map(data=> {
                    return (
                        <div className="ad-foot-info" key={data.id}>
                            <h1>Contact Information</h1>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-map-marker"></i> Address</div>
                                <div className="add-data">{data.address}</div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-phone-square"></i> Contact No.</div>
                                <div className="add-data">{data.mobile}</div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-envelope"></i> Email Id</div>
                                <div className="add-data">{data.email}</div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-globe"></i> Website</div>
                                <div className="add-data"><a href={data.website}
                                                             target="blank">{data.website}</a>
                                </div>
                            </div>
                            <div className="ad-foot-info-content">
                                <button className="ad-foot-edit" onClick={this.onConModalOpen}><i className="fa fa-pencil-square-o"></i> Edit</button>
                            </div>
                        </div>
                    )
                })}

                {this.state.hours.map(data=> {
                    return (
                        <div className="ad-foot-info" key={data.id}>
                            <h1>Opening Hours</h1>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-calendar"></i> Monday - Thrusday</div>
                                <div className="add-data">{data.montothru}</div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-calendar"></i> Firday</div>
                                <div className="add-data">{data.fri}</div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-calendar"></i> Saturday</div>
                                <div className="add-data">{data.sat}</div>
                            </div>
                            <div className="ad-foot-info-content">
                                <button className="ad-foot-edit" onClick={this.onHrModalOpen}><i className="fa fa-pencil-square-o"></i> Edit</button>
                            </div>
                        </div>
                    )
                })}

                {this.state.social.map(data=> {
                    return (
                        <div className="ad-foot-info" key={data.id}>
                            <h1>Social Media Links</h1>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-facebook-square"></i> Facebook</div>
                                <div className="add-data">
                                    <a href={data.fb} target="blank">www.facebook.com</a>
                                </div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-twitter-square"></i> Twitter</div>
                                <div className="add-data">
                                    <a href={data.tw} target="blank">www.twitter.com</a>
                                </div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-instagram"></i> Instagram</div>
                                <div className="add-data">
                                    <a href={data.insta} target="blank">www.instagram.com</a>
                                </div>
                            </div>
                            <div className="ad-foot-info-content">
                                <div className="add-label"><i className="fa fa-linkedin-square"></i> Linked In</div>
                                <div className="add-data">
                                    <a href={data.lin} target="blank">www.linkedin.com</a>
                                </div>
                            </div>
                            <div className="ad-foot-info-content">
                                <button className="ad-foot-edit" onClick={this.onSocModalOpen}><i className="fa fa-pencil-square-o"></i> Edit</button>
                            </div>
                        </div>
                    )
                })}

                <FooterLawfirm
                    open={this.state.lawOpen}
                    close={this.onLawModalClose}
                    data={this.state.lawfirm}
                />
                <FooterContact
                    open={this.state.conOpen}
                    close={this.onConModalClose}
                    data={this.state.contact}
                />
                <FooterHours
                    open={this.state.hrOpen}
                    close={this.onHrModalClose}
                    data={this.state.hours}
                />
                <FooterSocial
                    open={this.state.socOpen}
                    close={this.onSocModalClose}
                    data={this.state.social}
                />
                <div className='sweet-loading-foot'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}
export default AdminFooter;