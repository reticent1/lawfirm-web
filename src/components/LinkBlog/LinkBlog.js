import React from 'react';
import './LinkBlog.css';
import PostView from '../AdminPostView/PostView';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;
class LinkBlog extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            addPost:false,
            posts:[],
            views:[],
            viewPost:false,
            id:'',
            srch:'',
            data:false,
            loading:true
        }
    }

    fetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/all',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                if (data === 'no post'){
                    console.log('no post')
                }else{
                    this.setState({posts:data,data:true,loading:false})
                }
            })
            .catch(console.log)
    }

    componentDidMount(){
        this.fetcher();
    }

    onViewPostOpen=(key)=>{
        this.setState({viewPost:true})
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/view',{
            method:'post',
            headers:{'x-Trigger':'CORS','content-type':'application/json'},
            body:JSON.stringify({
                id:key
            })
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({views:data})
            })
            .catch(console.log)
    }

    onViewPostClose=()=>{
        this.setState({viewPost:false})
        this.fetcher();
    }

    onSearch=(event)=>{
        this.setState({srch:event.target.value})
    }
    render() {
        const filter=this.state.posts.filter(data=>{
            return(
                data.p_title
                    .toLowerCase().includes(this.state.srch.toLowerCase())
            )
        })
        return (
            <div className="b-post-container">
                <div className="b-search-div">
                    <i className="fa fa-search"/> <input type="search" placeholder="Search Blog" onChange={this.onSearch} className="b-search-control"/>
                </div>
                <div className="b-blog-head">
                    <span>Blog</span>
                </div>
                {filter.map(data =>{
                    return(
                        <div className="b-post-content" key={data.id}>
                            <div className="b-post-head" style={{backgroundImage: `url(${'https://secure-wave-27786.herokuapp.com/'+data.p_image})`}}>
                                <h3>{data.p_title}</h3>
                                <div>
                                    <div className="b-post-label">Published on: {data.p_date.substring(4,15)}</div>
                                    <div className="b-post-label">By: {data.p_writer}</div>
                                </div>
                            </div>
                            <div className="b-post-describe">
                                <h4>Summary</h4>
                                <p>
                                    {data.p_summary}
                                </p>
                            </div>
                            <div className="b-post-icons" onClick={()=>this.onViewPostOpen(data.id)}>
                                <i className="fa fa-eye"/> Read
                            </div>
                        </div>
                    )
                })}
                <PostView
                    open={this.state.viewPost}
                    close={this.onViewPostClose}
                    data={this.state.views}
                />
                <div className='sweet-loading-b'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}
export default LinkBlog;