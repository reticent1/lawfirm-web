import React from 'react';
import './Contactus.css';
import 'font-awesome/css/font-awesome.min.css';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class Contactus extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            fname:'',
            lname:'',
            mobile:'',
            email:'',
            message:'',
            contact:[],
            data:false,
            emailvalid:true,
            emailblank:true,
            fnblank:true,
            lnblank:true,
            mobblank:true,
            mobvalid:true,
            lnvalid:true,
            fnvalid:true,
            loading:true
        }
    }

    componentDidMount(){
        fetch('https://secure-wave-27786.herokuapp.com/contactinfo/info', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({contact:data,data:true,loading:false})
            })
            .catch(console.log)
    }

    getFname=(event)=>{
        this.setState({fname:event.target.value})
        if (event.target.value.trim() === "") {
            this.setState({fnblank: false, fnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({fnblank:true})
            if (event.target.value.length < 3) {
                this.setState({fnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({fnvalid: true, fname: event.target.value})
            }
        }
    }

    getLname=(event)=>{
        this.setState({lname:event.target.value})
        if (event.target.value.trim() === "") {
            this.setState({lnblank: false, lnvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({lnblank:true})
            if (event.target.value.length < 3) {
                this.setState({lnvalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({lnvalid: true, lname: event.target.value})
            }
        }
    }

    getMob=(event)=>{
        const mobValidator = /^\d{9}$/;
        this.setState({mobile:event.target.value})
        if (event.target.value.match(mobValidator)) {
            this.setState({
                mobile: event.target.value,
                mobvalid: true,
            })
        } else if (event.target.value.trim() === "") {
            this.setState({mobblank: false, mobvalid: true})
        } else {
            this.setState({mobvalid: false, mobblank: true})
        }
    }

    getEmail=(event)=>{
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        this.setState({email:event.target.value})
        if (event.target.value.match(mailformat)) {
            this.setState({emailvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({emailblank: false, emailvalid: true})
        } else {
            this.setState({emailvalid: false, emailblank: true})
        }
    }

    getMsg=(event)=>{
        this.setState({message:event.target.value})
    }

    onContactUs=()=>{
        const mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        const mobValidator = /^\d{9}$/;
        const {fname,lname,mobile,email}=this.state;
        if (fname === ""){
            this.setState({fnblank: false, fnvalid: true})
        }else if (lname === ""){
            this.setState({lnblank:false,lnvalid:true})
        }else if (mobile === ""){
            this.setState({mobblank:false,mobvalid:true})
        }else if(email === ""){
            this.setState({emailblank:false,emailvalid:true})
        }
        if(mobile.match(mobValidator) && email.match(mailformat) && fname !== '' && lname !== '') {
            fetch('https://secure-wave-27786.herokuapp.com/contactus', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    fname: this.state.fname,
                    lname: this.state.lname,
                    mobile: this.state.mobile,
                    email: this.state.email,
                    message: this.state.message
                })
            })
                .then(response=>response.json())
                .then(data=> {
                    if (data === 'error in insert') {
                        NotificationManager.error('Please try after some time')
                    } else {
                        this.setState({
                            fname: '',
                            lname: '',
                            mobile: '',
                            email: '',
                            message: '',
                        })
                        NotificationManager.success('Thank you!, We will get you soon.')
                    }
                })
                .catch(console.log)
        }
    }

    render() {
        return (
            <div className="contact-container">
                <div className="contact-head">
                    <span>Contact Us</span>
                </div>
                <div className="location wow fadeInUp">
                    <h1 className="h2">Contact Information</h1>
                    {this.state.contact.map((data,i)=>{
                        return(
                            <ul key={i}>
                                <li className="address">
                                    {data.address}
                                </li>
                                <li className="phone">
                                    {data.mobile}
                                </li>
                                <li className="mail">
                                    {data.email}
                                </li>
                                <li className="website">
                                    <a href={data.website} target="blank">{data.website}</a>
                                </li>
                            </ul>
                        )
                    })}
                </div>
                <div className="contact-form wow fadeInUp">
                    <h1 className="h1">Send Us a Message</h1>
                    <div className="form-content">
                        <input type="text" className="name-text"  value={this.state.fname} placeholder="First Name" onChange={this.getFname}/>
                        <input type="text" className="name-text" value={this.state.lname} placeholder="Last Name" onChange={this.getLname}/>
                    </div>
                    <div className="form-content">
                        <input type="text" className="form-control" value={this.state.mobile} placeholder="Mobile No. w/o country code" onChange={this.getMob}/>
                    </div>
                    <div className="form-content">
                        <input type="mail" className="form-control" placeholder="Email Id" value={this.state.email} onChange={this.getEmail}/>
                    </div>
                    <div className="form-content">
                        <textarea type="text" className="form-control" cols="30" rows="5" value={this.state.message} placeholder="Your Message (optional)" onChange={this.getMsg}/>
                    </div>
                    <div className="form-content">
                        <button className="send-btn" onClick={this.onContactUs}>Send</button>
                    </div>
                    <span hidden={this.state.emailblank} className="error-span">Please enter Email Id.</span>
                    <span hidden={this.state.emailvalid} className="error-span">Please enter correct Email Id.</span>
                    <span hidden={this.state.fnblank} className="error-span">Please enter First Name.</span>
                    <span hidden={this.state.lnblank} className="error-span">Please enter Last Name.</span>
                    <span hidden={this.state.mobblank} className="error-span">Please enter Mobile No.</span>
                    <span hidden={this.state.fnvalid} className="error-span">First Name must be 3 or more character.</span>
                    <span hidden={this.state.lnvalid} className="error-span">Last Name must be 3 or more character.</span>
                    <span hidden={this.state.mobvalid} className="error-span">Please enter valid Mobile No.</span>
                </div>
                <NotificationContainer/>
                <div className='sweet-loading-c'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}

export default Contactus;