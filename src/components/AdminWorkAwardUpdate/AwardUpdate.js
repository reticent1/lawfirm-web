import React from 'react';
import '../AdminWorkAward/AdminWorkAward.css'
import Modal from 'react-responsive-modal';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class AdminWorkAward extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            date:null,
            a_id:'',
            a_name:'',
            e_name:'',
            receive:'',
            describe:'',
            anblnk:true,
            enblnk:true,
            rcblnk:true,
            dsblnk:true,
            dtblnk:true
        }
    }

    handleDateChange=(event)=>{
        this.setState({date:event.target.value})
        if(event.target.value.trim() !== ''){
            this.setState({dtblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({dtblnk:false})
        }
    }
    getAward=(event)=>{
        this.setState({a_name:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({anblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({anblnk:false})
        }
    }
    getEvent=(event)=>{
        this.setState({e_name:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({enblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({enblnk:false})
        }
    }
    getReceiveForm=(event)=>{
        this.setState({receive:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({rcblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({rcblnk:false})
        }
    }
    getDescribe=(event)=>{
        this.setState({describe:event.target.value})
    }

    updateAward=()=>{
        const {a_name,e_name,date,receive}=this.state;
        if(a_name.trim() !== "" && e_name.trim() !== "" && date.trim() !== null && date.trim() !== "" && receive.trim() !== "") {
            fetch('https://secure-wave-27786.herokuapp.com/awards/update', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    a_name: this.state.a_name,
                    e_name: this.state.e_name,
                    e_date: this.state.date,
                    receive: this.state.receive,
                    describe: this.state.describe,
                    a_id: this.state.a_id
                })
            })
                .then(response=>response.json())
                .then(data=>{
                    if(data === 'not updated'){
                        NotificationManager.error("Sorry! Award not updated")
                    }else {
                        NotificationManager.success("Award Updated Successfully!");
                        this.props.close();
                    }
                })
                .catch(console.log)
        }if (a_name.trim() === ""){
            this.setState({anblnk:false})
        }else if(e_name.trim() === ""){
            this.setState({enblnk:false})
        }else if(date === null){
            this.setState({dtblnk:false})
        }else if(receive.trim() === ""){
            this.setState({rcblnk:false})
        }
    }

    getAllValues=()=>{
        let a_id,a_name,e_name,a_date,receive,describe;
        this.props.awardUpdate.map(data=>{
            return(
                a_id=data.a_id,
                a_name=data.a_name,
                    e_name=data.event_name,
                    a_date=data.a_date,
                    receive=data.receive_form,
                    describe=data.a_describe
            )
        })
        this.setState({
            a_id:a_id,
            a_name:a_name,
            date:a_date.substring(0,10),
            receive:receive,
            describe:describe,
            e_name:e_name
        })
    }

    render() {
        return (
            <div>
                <Modal
                    onEntered={this.getAllValues}
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div>
                        <h1 className="award-head">
                            Award Update
                        </h1>
                        <h3>#{this.state.a_id}</h3>
                        <div className="award-content">
                            <input type="text" className="award-control" placeholder="Award Name" onChange={this.getAward} value={this.state.a_name}/>
                        </div>
                        <div className="award-content">
                            <input type="text" className="award-control" placeholder="Event Name" onChange={this.getEvent} value={this.state.e_name}/>
                        </div>
                        <div className="award-content">
                            <input type="date" className="award-control" onChange={this.handleDateChange} value={this.state.date}/>
                        </div>
                        <div className="award-content">
                            <input type="text" className="award-control" placeholder="Award Received From" onChange={this.getReceiveForm} value={this.state.receive}/>
                        </div>
                        <div className="award-content">
                            <textarea className="award-control" cols="30" rows="5" placeholder="Award Description" onChange={this.getDescribe} value={this.state.describe}/>
                        </div>
                        <div className="award-content">
                            <button className="appo-btn" onClick={this.updateAward}>Update</button>
                        </div>
                    </div>
                    <span hidden={this.state.anblnk} className="error-span">Please enter Award Name</span>
                    <span hidden={this.state.enblnk} className="error-span">Please enter Event Name</span>
                    <span hidden={this.state.dtblnk} className="error-span">Please enter Date</span>
                    <span hidden={this.state.rcblnk} className="error-span">Please enter receive from</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}

export default AdminWorkAward;