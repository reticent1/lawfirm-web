import React from 'react';
import Modal from 'react-responsive-modal';
import './PracticeDelete.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class PracticeDelete extends React.Component {

    deletePractice = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/practice/delete', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                id: this.props.del
            })
        })
            .then(response=>response.json())
            .then(data=> {
                    this.props.close()
                    NotificationManager.info('Deleted Successfully!')
                }
            )
            .catch(console.log)
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <div className="del-container">
                        Are you sure you want to delete card?
                        <div className="del-btn">
                            <button onClick={this.deletePractice}>Yes</button>
                            <button onClick={this.props.close}>No</button>
                        </div>
                    </div>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default PracticeDelete;