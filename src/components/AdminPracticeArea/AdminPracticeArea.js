import React from 'react';
import './AdminPracticeArea.css';
import addCard from './add (2).png';
import PracticeAdd from '../AdminPracticeAreaAdd/PracticeAdd';
import PracticeUpdate from '../AdminPracticeAreaEdit/PracticeUpdate';
import PracticeDelete from '../AdminPracticeAreaDelete/PracticeDelete';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class AdminPracticeArea extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            addOpen:false,
            laws:[],
            searchLaw:'',
            updateOpen:false,
            view:[],
            delOpen:false,
            delId:'',
            loading:true
        }
    }

    fetcher=()=>{
        fetch('https://secure-wave-27786.herokuapp.com/practice/viewall',{
            method:'get',
            headers:{'content-type':'application/json'}
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({laws:data,loading:false})
            })
            .catch(console.log)
    }

    componentDidMount(){
        this.fetcher()
    }

    onAddModalOpen=()=>{
        this.setState({addOpen:true})
    }

    onAddModalClose=()=>{
        this.setState({addOpen:false})
        this.fetcher()
    }

    onUpdateOpen=(key)=>{
        this.setState({updateOpen:true})
        fetch('https://secure-wave-27786.herokuapp.com/practice/view',{
            method:'post',
            headers:{'content-type':'application/json'},
            body:JSON.stringify({
                id:key
            })
        })
            .then(response=>response.json())
            .then(data=>{
                this.setState({view:data})
            })
            .catch(console.log)
    }

    onUpdateClose=()=>{
        this.setState({updateOpen:false})
        this.fetcher()
    }

    getSearch=(event)=>{
        this.setState({searchLaw:event.target.value})
    }

    onDelOpen=(key)=>{
        this.setState({delOpen:true,delId:key})
    }

    onDelClose=()=>{
        this.setState({delOpen:false})
        this.fetcher()
    }


    render() {
        const filter=this.state.laws.filter(data=>{
            return(
                data.title
                    .toLowerCase().includes(this.state.searchLaw.toLowerCase())
            )
        })
        return (
            <div>
                <div className="search-div">
                    <i className="fa fa-search sr-icon">
                    </i>
                    <input type="search" onChange={this.getSearch} className="search-bar" placeholder="Search Laws"/>
                </div>
                <div className="admin-pr-container">
                    {filter.map(data=>{
                        return(
                            <div className="admin-pr-card" key={data.id}>
                                <h4>{data.title}</h4>
                                <p>{data.description}</p>
                                <div className="admin-pr-icons">
                                    <i className="fa fa-pencil-square-o edit" onClick={()=>this.onUpdateOpen(data.id)}></i>
                                    <i className="fa fa-trash-o delete" onClick={()=>this.onDelOpen(data.id)}></i>
                                </div>
                            </div>
                        )
                    })}

                    <div className="admin-pr-card">
                        <img src={addCard} className="add-icon" onClick={this.onAddModalOpen} alt="200x200"/>
                    </div>
                </div>
                <PracticeAdd
                    open={this.state.addOpen}
                    close={this.onAddModalClose}
                />
                <PracticeUpdate
                    open={this.state.updateOpen}
                    close={this.onUpdateClose}
                    view={this.state.view}
                />
                <PracticeDelete
                    open={this.state.delOpen}
                    close={this.onDelClose}
                    del={this.state.delId}
                />
                <div className='sweet-loading-area'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}

export default AdminPracticeArea;