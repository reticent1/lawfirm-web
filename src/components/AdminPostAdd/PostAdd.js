import React from 'react';
import './PostAdd.css';
import Modal from 'react-responsive-modal';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class PostAdd extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            p_title: '',
            p_writer: '',
            p_summary: '',
            p_details: '',
            p_image: null,
            p_date: '',
            ptblnk: true,
            ptvalid: true,
            pwblnk: true,
            psumblnk: true,
            psumvalid: true,
            pdtblnk: true,
            pdtvalid: true,
            imgblnk: true,
            imgvalid: true
        }
    }

    getTitle = (event)=> {
        this.setState({p_title: event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({ptblnk: true})
            if (event.target.value.length > 150) {
                this.setState({ptvalid: false})
            } else {
                this.setState({ptvalid: true})
            }
        } else {
            this.setState({ptblnk: false})
        }
    }
    getWriter = (event)=> {
        this.setState({p_writer: event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({pwblnk: true})
        } else {
            this.setState({pwblnk: false})
        }
    }
    getSummary = (event)=> {
        this.setState({p_summary: event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({psumblnk: true})
            if (event.target.value.length > 300) {
                this.setState({psumvalid: false})
            } else {
                this.setState({psumvalid: true})
            }
        } else {
            this.setState({psumblnk: false})
        }
    }
    getDetails = (event)=> {
        this.setState({p_details: event.target.value});
        if (event.target.value.trim() !== "") {
            this.setState({pdtblnk: true})
            if (event.target.value.length > 9999) {
                this.setState({pdtvalid: false})
            } else {
                this.setState({pdtvalid: true})
            }
        } else {
            this.setState({pdtblnk: false})
        }
    }
    getImage = (event)=> {
        this.setState({p_image: event.target.files[0]});
        if (event.target.value.trim() !== "") {
            this.setState({imgblnk: true})
        } else {
            this.setState({imgblnk: false})
        }
    }

    addPost = ()=> {
        const {p_title, p_writer, p_summary, p_details, p_image}=this.state;
        if (p_title.trim() !== "" &&
            p_title.length <= 150 &&
            p_summary.length <= 300 &&
            p_details.length <= 9999 &&
            p_writer.trim() !== "" &&
            p_summary.trim() !== "" &&
            p_details.trim() !== "" &&
            p_image !== null &&
            p_image.size <= 3145728) {
            const formData = new FormData();
            formData.append('p_title', this.state.p_title);
            formData.append('p_writer', this.state.p_writer);
            formData.append('p_details', this.state.p_details);
            formData.append('postImage', this.state.p_image);
            formData.append('p_summary', this.state.p_summary);
            formData.append('p_date', new Date());
            fetch('https://secure-wave-27786.herokuapp.com/blogpost/add', {
                method: 'post',
                body: formData
            })
                .then(data=> {
                    if (data === 'error') {
                        NotificationManager.error('Sorry! Post not created.')
                    } else {
                        this.props.close();
                        this.setState({
                            p_title: '',
                            p_writer: '',
                            p_summary: '',
                            p_details: '',
                            p_image: null,
                            p_date: '',
                            ptblnk: true,
                            ptvalid: true,
                            pwblnk: true,
                            psumblnk: true,
                            psumvalid: true,
                            pdtblnk: true,
                            pdtvalid: true,
                            imgblnk: true,
                            imgvalid: true
                        })
                        NotificationManager.success('Post created successfully!')
                    }
                })
                .catch(console.log)
        }
        if (p_title.trim() === "") {
            this.setState({ptblnk: false})
        } else if (p_writer.trim() === "") {
            this.setState({pwblnk: false})
        } else if (p_summary.trim() === "") {
            this.setState({psumblnk: false})
        } else if (p_details.trim() === "") {
            this.setState({pdtblnk: false})
        } else if (p_image === null) {
            this.setState({imgblnk: false})
        } else if (p_image.size > 3145728) {
            this.setState({imgvalid: false, imgblnk: true})
        } else if (p_image.size <= 3145728) {
            this.setState({imgvalid: true, imgblnk: true})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <h4 className="form-head">Create Post</h4>
                    <div className="add-post-content">
                        <input type="text" placeholder="Post Title" className="add-post-control"
                               onChange={this.getTitle}/>
                    </div>
                    <div className="add-post-content">
                        <input type="text" placeholder="Written by" className="add-post-control"
                               onChange={this.getWriter}/>
                    </div>
                    <div className="add-post-content">
                        <textarea type="text" placeholder="Short Summary" className="add-post-control" cols="50"
                                  rows="5" onChange={this.getSummary}/>
                    </div>
                    <div className="add-post-content">
                        <textarea type="text" placeholder="Detail Article" className="add-post-control" cols="100"
                                  rows="12" onChange={this.getDetails}/>
                    </div>
                    <div className="add-post-content">
                        <input type="file" onChange={this.getImage} accept="image/x-png,image/jpeg"/>
                    </div>
                    <div className="add-post-content">
                        <button className="appo-btn" onClick={this.addPost}>Create</button>
                    </div>
                    <span hidden={this.state.imgblnk} className="error-span">Please choose image.</span>
                    <span hidden={this.state.imgvalid} className="error-span">Please upload image below 3 MB.</span>
                    <span hidden={this.state.ptblnk} className="error-span">Please enter post title.</span>
                    <span hidden={this.state.ptvalid} className="error-span">Please enter post title within 150 characters.</span>
                    <span hidden={this.state.pwblnk} className="error-span">Please enter writer name.</span>
                    <span hidden={this.state.psumblnk} className="error-span">Please enter summary.</span>
                    <span hidden={this.state.psumvalid} className="error-span">Please enter summary within 300 characters.</span>
                    <span hidden={this.state.pdtblnk} className="error-span">Please enter details.</span>
                    <span hidden={this.state.pdtvalid} className="error-span">Please enter details within 999 characters.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default PostAdd;