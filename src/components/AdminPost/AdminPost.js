import React from 'react';
import './AdminPost.css';
import PostAdd from '../AdminPostAdd/PostAdd';
import PostEdit from '../AdminPostEdit/PostEdit';
import PostView from '../AdminPostView/PostView';
import PicPost from '../AdminPostPicChange/PicPost';
import PostDelete from '../AdminPostDelete/PostDelete';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class AdminPost extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addPost: false,
            posts: [],
            views: [],
            postSearch: '',
            editPost: false,
            viewPost: false,
            picPost: false,
            id: '',
            delPost: false,
            loading: true
        }
    }

    fetcher = ()=> {
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/all', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({posts: data, loading: false})
            })
            .catch(console.log)
    }

    componentDidMount() {
        this.fetcher();
    }

    onAddPostOpen = ()=> {
        this.setState({addPost: true})
    }

    onAddPostClose = ()=> {
        this.setState({addPost: false})
        this.fetcher();
    }
    onEditPostOpen = (key)=> {
        this.setState({editPost: true})
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/view', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                id: key
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({views: data})
            })
            .catch(console.log)
    }

    onEditPostClose = ()=> {
        this.setState({editPost: false})
        this.fetcher();
    }

    onViewPostOpen = (key)=> {
        this.setState({viewPost: true})
        fetch('https://secure-wave-27786.herokuapp.com/blogpost/view', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                id: key
            })
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({views: data})
            })
            .catch(console.log)
    }

    onViewPostClose = ()=> {
        this.setState({viewPost: false})
        this.fetcher();
    }

    onPostSearch = (event)=> {
        this.setState({postSearch: event.target.value})
    }

    onPicPostOpen = (key)=> {
        this.setState({picPost: true, id: key})
    }

    onPicPostClose = ()=> {
        this.setState({picPost: false})
        this.fetcher();
    }

    onDelPostOpen = (key)=> {
        this.setState({delPost: true, id: key})
    }

    onDelPostClose = ()=> {
        this.setState({delPost: false})
        this.fetcher();
    }

    render() {
        const filter = this.state.posts.filter(filt=> {
            return (
                filt.p_title
                    .concat(filt.p_writer)
                    .toLowerCase().includes(this.state.postSearch.toLowerCase())
            )
        })
        return (
            <div>
                <div className="admin-post-container">
                    <div className="attorney-search">
                        <input type="search" placeholder="Search Posts" className="search-control"
                               onChange={this.onPostSearch}/><i className="fa fa-search"></i>
                    </div>

                    {filter.map(data => {
                        return (
                            <div className="ad-post-content" key={data.id}>
                                <div className="ad-post-head"
                                     style={{backgroundImage: `url(${'https://secure-wave-27786.herokuapp.com/' + data.p_image})`}}>
                                    <h3>{data.p_title}</h3>
                                    <div>
                                        <div className="ad-post-label">Published
                                            on: {data.p_date.substring(4, 15)}</div>
                                        <div className="ad-post-label">By: {data.p_writer}</div>
                                    </div>
                                </div>
                                <div className="ad-post-describe">
                                    <h4>Summary</h4>
                                    <p>
                                        {data.p_summary}
                                    </p>
                                </div>
                                <div className="ad-post-icons">
                                    <i className="fa fa-eye" onClick={()=>this.onViewPostOpen(data.id)}></i>
                                    <i className="fa fa-pencil-square-o" onClick={()=>this.onEditPostOpen(data.id)}></i>
                                    <i className="fa fa-picture-o" onClick={()=>this.onPicPostOpen(data.id)}></i>
                                    <i className="fa fa-trash-o" onClick={()=>this.onDelPostOpen(data.id)}></i>
                                </div>
                            </div>
                        )
                    })}

                    <div className="ad-post-content">
                        <button className="ad-post-btn" onClick={this.onAddPostOpen}>
                            <i className="fa fa-plus-square-o"></i>
                            <p>Create Post</p>
                        </button>
                    </div>
                    <PostAdd
                        open={this.state.addPost}
                        close={this.onAddPostClose}
                    />
                    <PostEdit
                        open={this.state.editPost}
                        close={this.onEditPostClose}
                        data={this.state.views}
                    />
                    <PostView
                        open={this.state.viewPost}
                        close={this.onViewPostClose}
                        data={this.state.views}
                    />
                    <PicPost
                        open={this.state.picPost}
                        close={this.onPicPostClose}
                        id={this.state.id}
                    />
                    <PostDelete
                        open={this.state.delPost}
                        close={this.onDelPostClose}
                        id={this.state.id}
                    />
                </div>
                <div className='sweet-loading-post'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }

}
export default AdminPost;