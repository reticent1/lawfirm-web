import React from 'react';
import './AdminPanel.css';
import 'font-awesome/css/font-awesome.min.css'
import AppointmentImg from './adminicons/015-calendar.png';
import WorkImg from './adminicons/suitcase.png';
import PracticeImg from './adminicons/target.png';
import AttorneyImg from './adminicons/lawyer.png';
import PostImg from './adminicons/post-it.png';
import ContactImg from './adminicons/contact.png';
import FooterImg from './adminicons/article.png';
import SettingImg from './adminicons/working.png';
import ManageAppointments from '../ManageAppointments/ManageAppointments';
import AdminWork from '../AdminWork/AdminWork';
import AdminPracticeArea from '../AdminPracticeArea/AdminPracticeArea';
import AdminAttorney from '../AdminAttorney/AdminAttorney';
import AdminPost from '../AdminPost/AdminPost';
import AdminContact from '../AdminContact/AdminContact';
import AdminFooter from '../AdminFooter/AdminFooter';
import AdminProfile from '../AdminNavbar/AdminProfile';
import AdminSettings from '../AdminSettings/AdminSettings';
import AdSideDrawer from '../AdminSideDrawer/AdSideDrawer';
import BackDrop from '../BackDrop/BackDrop';

class AdminPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            adminRouter: 'dashboard',
            adSideDrawer: false,
            apoint: false,
            cont: false,
            work: false,
            pract: false,
            attr: false,
            post: false,
            foot: false,
            sett: false
        }
    }

    onAdminRouter = (val)=> {
        if (val === 'appointments') {
            this.setState({
                apoint: true,
                cont: false,
                work: false,
                pract: false,
                attr: false,
                post: false,
                foot: false,
                sett: false
            })
        } else if (val === 'contact') {
            this.setState({
                apoint: false,
                cont: true,
                work: false,
                pract: false,
                attr: false,
                post: false,
                foot: false,
                sett: false
            })
        } else if (val === 'workrecord') {
            this.setState({
                apoint: false,
                cont: false,
                work: true,
                pract: false,
                attr: false,
                post: false,
                foot: false,
                sett: false
            })
        } else if (val === 'practicearea') {
            this.setState({
                apoint: false,
                cont: false,
                work: false,
                pract: true,
                attr: false,
                post: false,
                foot: false,
                sett: false
            })
        } else if (val === 'attorney') {
            this.setState({
                apoint: false,
                cont: false,
                work: false,
                pract: false,
                attr: true,
                post: false,
                foot: false,
                sett: false
            })
        } else if (val === 'post') {
            this.setState({
                apoint: false,
                cont: false,
                work: false,
                pract: false,
                attr: false,
                post: true,
                foot: false,
                sett: false
            })
        } else if (val === 'footer') {
            this.setState({
                apoint: false,
                cont: false,
                work: false,
                pract: false,
                attr: false,
                post: false,
                foot: true,
                sett: false
            })
        } else if (val === 'setting') {
            this.setState({
                apoint: false,
                cont: false,
                work: false,
                pract: false,
                attr: false,
                post: false,
                foot: false,
                sett: true
            })
        }
        this.setState({
            adminRouter: val
        })
    }

    handelSideDrawer = ()=> {
        this.setState({adSideDrawer: !this.state.adSideDrawer})
    }

    handelBackDrop = ()=> {
        this.setState({adSideDrawer: false})
    }

    render() {
        let backdrop;
        if (this.state.adSideDrawer) {
            backdrop = <BackDrop closeBackDrop={this.handelBackDrop}/>
        }
        return (
            <div>
                <AdminProfile
                    drawerHandler={this.handelSideDrawer}
                    data={this.props.data}
                    router={this.props.router}
                />
                <AdSideDrawer
                    show={this.state.adSideDrawer}
                    router={this.props.router}
                    adRouter={this.onAdminRouter}
                    apoint={this.state.apoint}
                    cont={this.state.cont}
                    work={this.state.work}
                    pract={this.state.pract}
                    attr={this.state.attr}
                    post={this.state.post}
                    foot={this.state.foot}
                    sett={this.state.sett}
                />
                {backdrop}
                <div className="side-navbar">
                    <p className="dashboard" onClick={()=>this.onAdminRouter('dashboard')}>Dashboard</p>
                    <ul>
                        <li className={`${this.state.apoint ? "appoint active-sn" : "appoint"}`}
                            onClick={()=>this.onAdminRouter('appointments')}>Appointments
                        </li>
                        <li className={`${this.state.cont ? "contactus active-sn" : "contactus"}`}
                            onClick={()=>this.onAdminRouter('contact')}>Contact Us
                        </li>
                        <li className={`${this.state.work ? "work active-sn" : "work"}`}
                            onClick={()=>this.onAdminRouter('workrecord')}>Work
                        </li>
                        <li className={`${this.state.pract ? "practice-area active-sn" : "practice-area"}`}
                            onClick={()=>this.onAdminRouter('practicearea')}>Practice Area
                        </li>
                        <li className={`${this.state.attr ? "attorney active-sn" : "attorney"}`}
                            onClick={()=>this.onAdminRouter('attorney')}>Attorney
                        </li>
                        <li className={`${this.state.post ? "post active-sn" : "post"}`}
                            onClick={()=>this.onAdminRouter('post')}>Post
                        </li>
                        <li className={`${this.state.foot ? "footer active-sn" : "footer"}`}
                            onClick={()=>this.onAdminRouter('footer')}>Footer
                        </li>
                        <li className={`${this.state.sett ? "setting active-sn" : "setting"}`}
                            onClick={()=>this.onAdminRouter('setting')}>Settings
                        </li>
                    </ul>
                </div>
                {this.state.adminRouter === 'dashboard' ?
                    <div className="main-content">
                        <div className="admin-cards"
                             data-text={`Check your appointments, accept the appointments or postpone to schedule date.`}
                             onClick={()=> this.onAdminRouter('appointments')}
                        >
                            <div className="admin-icons">
                                <img src={AppointmentImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Manage Appointments</h4>
                            </div>
                        </div>
                        <div className="admin-cards"
                             data-text={`Check how many peoples want's you to contact them.`}
                             onClick={()=>this.onAdminRouter('contact')}
                        >
                            <div className="admin-icons">
                                <img src={ContactImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Contact Clients</h4>
                            </div>
                        </div>
                        <div
                            className="admin-cards"
                            data-text={`Edit Clients, Successfull cases and Honars & Awards history.`}
                            onClick={()=>this.onAdminRouter('workrecord')}
                        >
                            <div className="admin-icons">
                                <img src={WorkImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Edit Work Record</h4>
                            </div>
                        </div>
                        <div className="admin-cards"
                             data-text={`Add your practice law or edit exisiting laws.`}
                             onClick={()=>this.onAdminRouter('practicearea')}
                        >
                            <div className="admin-icons">
                                <img src={PracticeImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Practice Area</h4>
                            </div>
                        </div>
                        <div className="admin-cards"
                             data-text={`Register a new Attorney, edit or remove existing users.`}
                             onClick={()=>this.onAdminRouter('attorney')}
                        >
                            <div className="admin-icons">
                                <img src={AttorneyImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Manage Attorney</h4>
                            </div>
                        </div>
                        <div className="admin-cards"
                             data-text={`Manage your blog post, by adding the new post or remove existing posts.`}
                             onClick={()=>this.onAdminRouter('post')}
                        >
                            <div className="admin-icons">
                                <img src={PostImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Manage Blog Post</h4>
                            </div>
                        </div>

                        <div className="admin-cards"
                             data-text={`Change your social media links, contact information and working hours.`}
                             onClick={()=>this.onAdminRouter('footer')}
                        >
                            <div className="admin-icons">
                                <img src={FooterImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Edit Footer</h4>
                            </div>
                        </div>
                        <div className="admin-cards"
                             data-text={`Your account settings change password or username.`}
                             onClick={()=>this.onAdminRouter('setting')}
                        >
                            <div className="admin-icons">
                                <img src={SettingImg} alt="70x70"/>
                            </div>
                            <div className="cards-head">
                                <h4>Setting</h4>
                            </div>
                        </div>
                    </div>
                    : null}

                {this.state.adminRouter === 'appointments' ?
                    <ManageAppointments/> : null
                }

                {this.state.adminRouter === 'workrecord' ?
                    <AdminWork/> : null
                }
                {this.state.adminRouter === 'practicearea' ?
                    <AdminPracticeArea/> : null
                }
                {this.state.adminRouter === 'attorney' ?
                    <AdminAttorney/> : null
                }
                {this.state.adminRouter === 'post' ?
                    <AdminPost/> : null
                }
                {this.state.adminRouter === 'contact' ?
                    <AdminContact/> : null
                }
                {this.state.adminRouter === 'footer' ?
                    <AdminFooter/> : null
                }
                {this.state.adminRouter === 'setting' ?
                    <AdminSettings
                        data={this.props.data}
                        router={this.props.router}
                    /> : null
                }
            </div>
        );
    }

}

export default AdminPanel;