import React from 'react';
import './PracticeArea.css';
import law1 from './icons/act.svg';
import law2 from './icons/handcuffs.svg'
import law3 from './icons/law-book.svg'
import law4 from './icons/padlock.svg';
import law5 from './icons/agreement.svg';
import law6 from './icons/oath.svg';
import {PropagateLoader} from 'react-spinners';
import {css} from 'emotion';

const override = css`
    display: block;
    margin: 5px auto;
    border-color: red;
`;

class PracticeArea extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            laws: [],
            data:false,
            loading:true
        }
    }

    componentDidMount() {
        fetch('https://secure-wave-27786.herokuapp.com/practice/viewall', {
            method: 'get',
            headers: {
                'content-type': 'application/json',
            }
        })
            .then(response=>response.json())
            .then(data=> {
                this.setState({laws: data,data:true,loading:false})
            })
            .catch(console.log)
    }


    render() {

        const
            images = [
                {
                    img: law1
                },
                {
                    img: law2
                },
                {
                    img: law3
                },
                {
                    img: law4
                },
                {
                    img: law5
                },
                {
                    img: law6
                }
            ]

        let hide;
        if(this.state.laws.length === 6 || this.state.laws.length < 6){
            hide=true;
        }else {
            hide=false
        }
        return (
            <div>
                <div className="practice-container wow fadeInUp" data-wow-delay="0s">
                    <div className="practice-head">
                        <span>Practice Area</span>
                    </div>
                    {this.state.laws.slice(0,6).map((data,i)=> {
                        return (
                            <div className="practice-cards" key={i}>
                                <div className="practice-icon">
                                    <img src={images[i].img} alt="80*80"/>
                                </div>
                                <div className="practice-details">
                                    <h1>{data.title}</h1>
                                    <p>{data.description}</p>
                                </div>
                            </div>
                        )
                    })}
                    <div className="explore-btn" hidden={hide}>
                        <span onClick={()=>this.props.router('practice')}>explore More <i className="fa fa-long-arrow-right"/></span>
                    </div>
                </div>
                <div className='sweet-loading-pa'>
                    <PropagateLoader
                        className={override}
                        sizeUnit={"px"}
                        size={15}
                        color={'#958C8C'}
                        loading={this.state.loading}
                    />
                </div>
            </div>
        );
    }
}

export default PracticeArea;