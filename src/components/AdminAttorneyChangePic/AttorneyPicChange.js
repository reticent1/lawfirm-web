import React from 'react';
import './AttorneyPicChange.css';
import Modal from 'react-responsive-modal';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class AttorneyPicChange extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            file:null,
            pics:null,
            imgblnk:true,
            imgvalid:true
        }
    }

    chooseImage=(event)=>{
        this.setState({
            file:event.target.files[0],
           pics:URL.createObjectURL(event.target.files[0])
        })
        if (event.target.value.trim() !== ""){
            this.setState({imgblnk:true})
        }else{
            this.setState({imgblnk:false})
        }
    }

    changePic=()=>{
        const {file}=this.state;
        if(file !== null){
            if(file.size <= 3145728 ){
                const formData = new FormData();
                formData.append('attorneyImage',this.state.file);
                formData.append('id',this.props.id);
                fetch('https://secure-wave-27786.herokuapp.com/attorney/update/image',{
                    method:'post',
                    body:formData
                })
                    .then(data=>{
                        if(data === 'failure'){
                            NotificationManager.error("Sorry! image not update.")
                        }else{
                            this.props.close();
                            this.setState({file:null,
                                pics:null,
                                imgblnk:true,
                                imgvalid:true})
                            NotificationManager.success('Image updated successfully!')
                        }
                    })
                    .catch(console.log)
            }
        }
        if (file === null){
            this.setState({imgblnk:false})
        }else if(file.size >= 3145728){
            this.setState({imgvalid:false,imgblnk:true})
        }else if(file.size <= 3145728){
            this.setState({imgvalid:true,imgblnk:true})
        }

    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                >
                    <h3 className="form-head">Change Picture</h3>
                    <div className="picture-content">
                        <img src={this.state.pics} className="select-img" alt="150x150"/>
                        <input type="file" onChange={this.chooseImage} accept="image/x-png,image/jpeg" className="picture-control"/>
                    </div>
                    <button className="appo-btn" onClick={this.changePic}>Upload</button>
                    <span hidden={this.state.imgblnk} className="error-span">Please choose image.</span>
                    <span hidden={this.state.imgvalid} className="error-span">Please upload image below 3 MB.</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}
export default AttorneyPicChange;