import React from 'react';
import './AdminWorkAward.css';
import Modal from 'react-responsive-modal';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import {SingleDatePicker} from 'react-dates';
import {NotificationContainer,NotificationManager} from 'react-notifications';

class AdminWorkAward extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            date:null,
            focused:null,
            a_name:'',
            e_name:'',
            receive:'',
            describe:'',
            anblnk:true,
            enblnk:true,
            rcblnk:true,
            dsblnk:true,
            dtblnk:true
        }
    }

    handleDateChange=(date)=>{
        this.setState({date:date})
        if (date !== null){
            this.setState({dtblnk:true})
        }else if(date === null){
            this.setState({dtblnk:false})
        }
    }
    getAward=(event)=>{
        this.setState({a_name:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({anblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({anblnk:false})
        }
    }
    getEvent=(event)=>{
        this.setState({e_name:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({enblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({enblnk:false})
        }
    }
    getReceiveForm=(event)=>{
        this.setState({receive:event.target.value})
        if(event.target.value.trim() !== ""){
            this.setState({rcblnk:true})
        }else if(event.target.value.trim() === ""){
            this.setState({rcblnk:false})
        }
    }
    getDescribe=(event)=>{
        this.setState({describe:event.target.value})
    }

    addAward=()=>{
        const {a_name,e_name,date,receive}=this.state;
        if(a_name.trim() !== "" && e_name.trim() !== "" && date !== null && receive.trim() !== ""){
            fetch('https://secure-wave-27786.herokuapp.com/award/add',{
                method:'post',
                headers:{'content-type':'application/json'},
                body:JSON.stringify({
                    a_name:this.state.a_name,
                    e_name:this.state.e_name,
                    e_date:this.state.date,
                    receive:this.state.receive,
                    describe:this.state.describe,
                    entry_date:new Date()
                })
            })
                .then(response=>response.json())
                .then(data=>{
                    if(data === 'Not inserted'){
                        NotificationManager.error("Sorry! Award not Added")
                    }else{
                        NotificationManager.success("Award added Successfully!")
                        this.props.close()
                    }
                })
                .catch(console.log)
        }if (a_name.trim() === ""){
            this.setState({anblnk:false})
        }else if(e_name.trim() === ""){
            this.setState({enblnk:false})
        }else if(date === null){
            this.setState({dtblnk:false})
        }else if(receive.trim() === ""){
            this.setState({rcblnk:false})
        }
    }

    render() {
        return (
            <div>
                <Modal
                    onClose={this.props.close}
                    open={this.props.open}
                    center
                    classNames={{
                        modal: 'modal',
                        closeButton: 'close-btn'
                    }}
                    animationDuration={1000}
                >
                    <div>
                        <h1 className="award-head">
                            Awards Details
                        </h1>
                        <div className="award-content">
                            <input type="text" className="award-control" placeholder="Award Name" onChange={this.getAward}/>
                        </div>
                        <div className="award-content">
                            <input type="text" className="award-control" placeholder="Event Name" onChange={this.getEvent}/>
                        </div>
                        <div className="award-content">
                            <SingleDatePicker
                                isOutsideRange={() => false}
                                showClearDate={true}
                                small={true}
                                block={true}
                                regular={false}
                                numberOfMonths={1}
                                date={this.state.date}
                                onDateChange={date => this.handleDateChange(date)}
                                focused={this.state.focused}
                                onFocusChange={({ focused }) =>
                                    (this.setState({ focused }))
                                }
                                displayFormat="DD/MM/YYYY"
                                openDirection="down"
                                hideKeyboardShortcutsPanel={true}
                                showDefaultInputIcon={false}
                                reopenPickerOnClearDate={true}
                                readOnly={true}
                            />
                        </div>
                        <div className="award-content">
                            <input type="text" className="award-control" placeholder="Award Received From" onChange={this.getReceiveForm}/>
                        </div>
                        <div className="award-content">
                            <textarea className="award-control" cols="30" rows="5" placeholder="Award Description (Optional)" onChange={this.getDescribe}/>
                        </div>
                        <div className="award-content">
                            <button className="appo-btn" onClick={this.addAward}>Add Award</button>
                        </div>
                    </div>
                    <span hidden={this.state.anblnk} className="error-span">Please enter Award Name</span>
                    <span hidden={this.state.enblnk} className="error-span">Please enter Event Name</span>
                    <span hidden={this.state.dtblnk} className="error-span">Please enter Date</span>
                    <span hidden={this.state.rcblnk} className="error-span">Please enter receive from</span>
                </Modal>
                <NotificationContainer/>
            </div>
        );
    }

}

export default AdminWorkAward;